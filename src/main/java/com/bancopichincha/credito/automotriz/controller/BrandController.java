package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.service.BrandService;
import com.bancopichincha.credito.automotriz.service.dto.BrandDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.HttpStatus.*;

@Slf4j
@RestController
@RequestMapping("/api/brand")
public class BrandController {

    private final BrandService brandService;

    public BrandController(@Autowired BrandService brandService) {
        this.brandService = brandService;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<BrandDto>> getList() {
        log.info("Enter to getList");
        List<BrandDto> list = brandService.getList();
        return new ResponseEntity<>(list, OK);
    }

    @GetMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<BrandDto> getById(@PathVariable Integer id) {
        log.info("Enter to getById: " + id.toString());
        BrandDto brandDto = brandService.getById(id);
        return new ResponseEntity<>(brandDto, OK);
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<BrandDto> create(@Valid @RequestBody BrandDto brandDto) {
        log.info("Enter to create: " + brandDto.getName());
        brandDto = brandService.create(brandDto);
        return new ResponseEntity<>(brandDto, CREATED);
    }

    @PutMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<BrandDto> update(@Valid @RequestBody BrandDto brandDto) {
        log.info("Enter to update: " + brandDto.getId());
        brandDto = brandService.update(brandDto);
        return new ResponseEntity<>(brandDto, OK);
    }

    @PatchMapping(value = "/{id}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<BrandDto> edit(@PathVariable Integer id, @RequestBody Map<Object, Object> fields) {
        log.info("Enter to edit: " + id);
        BrandDto brandDto = brandService.edit(id, fields);
        return new ResponseEntity<>(brandDto, OK);
    }

    @DeleteMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(NO_CONTENT)
    public void delete(@PathVariable Integer id) {
        log.info("Enter to delete: " + id);
        brandService.deleteById(id);
    }

}
