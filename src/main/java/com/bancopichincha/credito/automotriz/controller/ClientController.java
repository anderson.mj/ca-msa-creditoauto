package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.service.ClientService;
import com.bancopichincha.credito.automotriz.service.dto.ClientDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.HttpStatus.*;

@Slf4j
@RestController
@RequestMapping("/api/client")
public class ClientController {

    private final ClientService clientService;

    public ClientController(@Autowired ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ClientDto>> getList() {
        log.info("Enter to getList");
        List<ClientDto> list = clientService.getList();
        return new ResponseEntity<>(list, OK);
    }

    @GetMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientDto> getById(@PathVariable Integer id) {
        log.info("Enter to getById: " + id.toString());
        ClientDto clientDto = clientService.getById(id);
        return new ResponseEntity<>(clientDto, OK);
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientDto> create(@Valid @RequestBody ClientDto clientDto) {
        log.info("Enter to create: " + clientDto.getIdentification());
        clientDto = clientService.create(clientDto);
        return new ResponseEntity<>(clientDto, CREATED);
    }

    @PutMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientDto> update(@Valid @RequestBody ClientDto clientDto) {
        log.info("Enter to update: " + clientDto.getId());
        clientDto = clientService.update(clientDto);
        return new ResponseEntity<>(clientDto, OK);
    }

    @PatchMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientDto> edit(@PathVariable Integer id, @RequestBody Map<Object, Object> fields) {
        log.info("Enter to edit: " + id);
        ClientDto clientDto = clientService.edit(id, fields);
        return new ResponseEntity<>(clientDto, OK);
    }

    @DeleteMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(NO_CONTENT)
    public void delete(@PathVariable Integer id) {
        log.info("Enter to delete: " + id );
        clientService.deleteById(id);
    }


}
