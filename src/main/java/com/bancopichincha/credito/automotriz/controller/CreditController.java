package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.service.CreditService;
import com.bancopichincha.credito.automotriz.service.dto.CreditDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RestController
@RequestMapping("/api/credit")
public class CreditController {

    private final CreditService creditService;

    public CreditController(@Autowired CreditService creditService) {
        this.creditService = creditService;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CreditDto>> getList() {
        log.info("Enter to getList");
        List<CreditDto> list = creditService.getList();
        return new ResponseEntity<>(list, OK);
    }

    @GetMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CreditDto> getById(@PathVariable Integer id) {
        log.info("Enter to getById: " + id.toString());
        CreditDto creditDto = creditService.getById(id);
        return new ResponseEntity<>(creditDto, OK);
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CreditDto> create(@Valid @RequestBody CreditDto creditDto) {
        log.info("Enter to create credit to client: " + creditDto.getClient().getId() + " - for vehicle: " + creditDto.getVehicle().getId());
        creditDto = creditService.create(creditDto);
        return new ResponseEntity<>(creditDto, CREATED);
    }

    @PutMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CreditDto> update(@Valid @RequestBody CreditDto creditDto) {
        log.info("Enter to update: " + creditDto.getId());
        creditDto = creditService.update(creditDto);
        return new ResponseEntity<>(creditDto, OK);
    }

    @PatchMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CreditDto> edit(@PathVariable Integer id, @RequestBody Map<Object, Object> fields) {
        log.info("Enter to edit: " + id);
        CreditDto creditDto = creditService.edit(id, fields);
        return new ResponseEntity<>(creditDto, OK);
    }

    @DeleteMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(NO_CONTENT)
    public void delete(@PathVariable Integer id) {
        log.info("Enter to delete: " + id );
        creditService.deleteById(id);
    }
}
