package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.service.ExecutiveService;
import com.bancopichincha.credito.automotriz.service.dto.ExecutiveDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RestController
@RequestMapping("/api/executive")
public class ExecutiveController {

    private final ExecutiveService executiveService;

    public ExecutiveController(@Autowired ExecutiveService executiveService) {
        this.executiveService = executiveService;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ExecutiveDto>> getList() {
        log.info("Enter to getList");
        List<ExecutiveDto> list = executiveService.getList();
        return new ResponseEntity<>(list, OK);
    }

    @GetMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ExecutiveDto> getById(@PathVariable Integer id) {
        log.info("Enter to getById: " + id.toString());
        ExecutiveDto executiveDto = executiveService.getById(id);
        return new ResponseEntity<>(executiveDto, OK);
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ExecutiveDto> create(@Valid @RequestBody ExecutiveDto executiveDto) {
        log.info("Enter to create: " + executiveDto.getIdentification());
        executiveDto = executiveService.create(executiveDto);
        return new ResponseEntity<>(executiveDto, CREATED);
    }

    @PutMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ExecutiveDto> update(@Valid @RequestBody ExecutiveDto executiveDto) {
        log.info("Enter to update: " + executiveDto.getId());
        executiveDto = executiveService.update(executiveDto);
        return new ResponseEntity<>(executiveDto, OK);
    }

    @PatchMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ExecutiveDto> edit(@PathVariable Integer id, @RequestBody Map<Object, Object> fields) {
        log.info("Enter to edit: " + id);
        ExecutiveDto executiveDto = executiveService.edit(id, fields);
        return new ResponseEntity<>(executiveDto, OK);
    }

    @DeleteMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(NO_CONTENT)
    public void delete(@PathVariable Integer id) {
        log.info("Enter to delete: " + id );
        executiveService.deleteById(id);
    }
}
