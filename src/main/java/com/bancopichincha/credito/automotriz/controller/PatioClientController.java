package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.service.PatioClientService;
import com.bancopichincha.credito.automotriz.service.dto.ClientDto;
import com.bancopichincha.credito.automotriz.service.dto.PatioClientDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RestController
@RequestMapping("/api/patioClient")
public class PatioClientController {

    private final PatioClientService patioClientService;

    public PatioClientController(@Autowired PatioClientService patioClientService) {
        this.patioClientService = patioClientService;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PatioClientDto>> getList() {
        log.info("Enter to getList");
        List<PatioClientDto> list = patioClientService.getList();
        return new ResponseEntity<>(list, OK);
    }

    @GetMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PatioClientDto> getById(@PathVariable Integer id) {
        log.info("Enter to getById: " + id.toString());
        PatioClientDto patioClientDto = patioClientService.getById(id);
        return new ResponseEntity<>(patioClientDto, OK);
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PatioClientDto> create(@Valid @RequestBody PatioClientDto patioClientDto) {
        int clientId = patioClientDto.getClientDto().getId();
        int patioId = patioClientDto.getPatioDto().getId();
        log.info("Enter to create: client - " + clientId + " : patio - " + patioId);
        patioClientDto = patioClientService.create(patioClientDto);
        return new ResponseEntity<>(patioClientDto, CREATED);
    }

    @PutMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PatioClientDto> update(@Valid @RequestBody PatioClientDto patioClientDto) {
        log.info("Enter to update: " + patioClientDto.getId());
        patioClientDto = patioClientService.update(patioClientDto);
        return new ResponseEntity<>(patioClientDto, OK);
    }

    @PatchMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PatioClientDto> edit(@PathVariable Integer id, @RequestBody Map<Object, Object> fields) {
        log.info("Enter to edit: " + id);
        PatioClientDto patioClientDto = patioClientService.edit(id, fields);
        return new ResponseEntity<>(patioClientDto, OK);
    }

    @DeleteMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(NO_CONTENT)
    public void delete(@PathVariable Integer id) {
        log.info("Enter to delete: " + id );
        patioClientService.deleteById(id);
    }
}
