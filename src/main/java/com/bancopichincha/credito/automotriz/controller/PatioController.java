package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.service.PatioService;
import com.bancopichincha.credito.automotriz.service.dto.PatioDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.HttpStatus.*;

@Slf4j
@RestController
@RequestMapping("/api/patio")
public class PatioController {

    private final PatioService patioService;

    public PatioController(@Autowired PatioService patioService) {
        this.patioService = patioService;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PatioDto>> getList() {
        log.info("Enter to getList");
        List<PatioDto> list = patioService.getList();
        return new ResponseEntity<>(list, OK);
    }

    @GetMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PatioDto> getById(@PathVariable Integer id) {
        log.info("Enter to getById: " + id);
        PatioDto patioDto = patioService.getByIdDto(id);
        return new ResponseEntity<>(patioDto, OK);
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PatioDto> create(@Valid @RequestBody PatioDto patioDto) {
        log.info("Enter to create: " + patioDto.getName());
        patioDto = patioService.create(patioDto);
        return new ResponseEntity<>(patioDto, CREATED);
    }

    @PutMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PatioDto> update(@Valid @RequestBody PatioDto patioDto) {
        log.info("Enter to update: " + patioDto.getId());
        patioDto = patioService.update(patioDto);
        return new ResponseEntity<>(patioDto, OK);
    }

    @PatchMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<PatioDto> edit(@PathVariable Integer id, @RequestBody Map<Object, Object> fields) {
        log.info("Enter to edit: " + id);
        PatioDto patioDto = patioService.edit(id, fields);
        return new ResponseEntity<>(patioDto, OK);
    }

    @DeleteMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(NO_CONTENT)
    public void delete(@PathVariable Integer id) {
        log.info("Enter to delete: " + id);
        patioService.deleteById(id);
    }

}
