package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.service.VehicleService;
import com.bancopichincha.credito.automotriz.service.dto.VehicleDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RestController
@RequestMapping("/api/vehicle")
public class VehicleController {

    private final VehicleService vehicleService;

    public VehicleController(@Autowired VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<VehicleDto>> getList() {
        log.info("Enter to getList");
        List<VehicleDto> list = vehicleService.getList();
        return new ResponseEntity<>(list, OK);
    }

    @GetMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<VehicleDto> getById(@PathVariable Integer id) {
        log.info("Enter to getById: " + id.toString());
        VehicleDto executiveDto = vehicleService.getById(id);
        return new ResponseEntity<>(executiveDto, OK);
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<VehicleDto> create(@Valid @RequestBody VehicleDto vehicleDto) {
        log.info("Enter to create: " + vehicleDto.getPlaca());
        vehicleDto = vehicleService.create(vehicleDto);
        return new ResponseEntity<>(vehicleDto, CREATED);
    }

    @PutMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<VehicleDto> update(@Valid @RequestBody VehicleDto vehicleDto) {
        log.info("Enter to update: " + vehicleDto.getId());
        vehicleDto = vehicleService.update(vehicleDto);
        return new ResponseEntity<>(vehicleDto, OK);
    }

    @PatchMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<VehicleDto> edit(@PathVariable Integer id, @RequestBody Map<Object, Object> fields) {
        log.info("Enter to edit: " + id);
        VehicleDto vehicleDto = vehicleService.edit(id, fields);
        return new ResponseEntity<>(vehicleDto, OK);
    }

    @DeleteMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(NO_CONTENT)
    public void delete(@PathVariable Integer id) {
        log.info("Enter to delete: " + id );
        vehicleService.deleteById(id);
    }

    @GetMapping(value = "/byBrand/{brandId}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<VehicleDto>> byBrandId(@PathVariable("brandId") Integer brandId) {
        log.info("Enter to byBrandId: " + brandId);
        List<VehicleDto> vehiclesDto = vehicleService.getByBrandId(brandId);
        return new ResponseEntity<>(vehiclesDto, OK);
    }

    @GetMapping(value = "/byModel/{model}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<VehicleDto>> byModel(@PathVariable("model")String model) {
        log.info("Enter to byModel: " + model);
        List<VehicleDto> vehiclesDto = vehicleService.getByModel(model);
        return new ResponseEntity<>(vehiclesDto, OK);
    }

}
