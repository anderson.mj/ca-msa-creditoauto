package com.bancopichincha.credito.automotriz.domain.enums;

import javax.persistence.AttributeConverter;
import java.util.stream.Stream;

public class CivilStatusConverter implements AttributeConverter<CivilStatus, String> {

    @Override
    public String convertToDatabaseColumn(CivilStatus civilStatus) {
        if (civilStatus == null) {
            return null;
        }
        return civilStatus.getStatus();
    }

    @Override
    public CivilStatus convertToEntityAttribute(String civilStatus) {
        if (civilStatus == null) {
            return null;
        }

        return Stream.of(CivilStatus.values())
                .filter(cs -> cs.getStatus().equals(civilStatus))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
