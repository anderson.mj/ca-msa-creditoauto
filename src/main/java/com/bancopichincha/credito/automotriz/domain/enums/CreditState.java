package com.bancopichincha.credito.automotriz.domain.enums;

import lombok.Getter;

@Getter
public enum CreditState {
    REGISTERED("REGISTERED"), SHIPPED("SHIPPED"), CANCELED("CANCELED");

    private final String state;

    private CreditState(String state) {
        this.state = state;
    }

    public static boolean isCreditState(Object value) {
        return (value instanceof String
                && (((String) value).equalsIgnoreCase(REGISTERED.getState()))
                || (((String) value).equalsIgnoreCase(SHIPPED.getState()))
                || (((String) value).equalsIgnoreCase(CANCELED.getState())));
    }
}
