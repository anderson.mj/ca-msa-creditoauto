package com.bancopichincha.credito.automotriz.domain.enums;

import javax.persistence.AttributeConverter;
import java.util.stream.Stream;

public class CreditStateConverter implements AttributeConverter<CreditState, String> {
    @Override
    public String convertToDatabaseColumn(CreditState creditState) {
        if (creditState == null) {
            return null;
        }
        return creditState.getState();
    }

    @Override
    public CreditState convertToEntityAttribute(String creditState) {
        if (creditState == null) {
            return null;
        }

        return Stream.of(CreditState.values())
                .filter(cs -> cs.equals(creditState))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
