package com.bancopichincha.credito.automotriz.domain.enums;

import lombok.Getter;

@Getter
public enum VehicleState {
    SOLD("SOLD"), FREE("FREE");

    private final String status;

    VehicleState(String status) {
        this.status = status;
    }

    private boolean isSold(VehicleState state) {
        return state.getStatus().equals(VehicleState.SOLD.status);
    }

    public static boolean isVehicleState(Object value) {
        return (value instanceof String
                && (((String) value).equalsIgnoreCase(SOLD.getStatus()))
                || (((String) value).equalsIgnoreCase(FREE.getStatus())));
    }
}
