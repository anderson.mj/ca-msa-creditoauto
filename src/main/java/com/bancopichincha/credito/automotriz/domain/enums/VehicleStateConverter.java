package com.bancopichincha.credito.automotriz.domain.enums;

import javax.persistence.AttributeConverter;
import java.util.stream.Stream;

public class VehicleStateConverter implements AttributeConverter<VehicleState, String> {

    @Override
    public String convertToDatabaseColumn(VehicleState state) {
        if (state == null) {
            return null;
        }
        return state.getStatus();
    }

    @Override
    public VehicleState convertToEntityAttribute(String state) {
        if (state == null) {
            return null;
        }

        return Stream.of(VehicleState.values())
                .filter(status -> status.getStatus().equals(state))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
