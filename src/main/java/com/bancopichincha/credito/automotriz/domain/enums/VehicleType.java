package com.bancopichincha.credito.automotriz.domain.enums;

import lombok.Getter;

@Getter
public enum VehicleType {
    SUV("SUV");

    private final String type;

    private VehicleType(String type) {
        this.type = type;
    }

    public static boolean isVehicleType(Object value) {
        return (value instanceof String
                && (((String) value).equalsIgnoreCase(SUV.getType())));
    }
}
