package com.bancopichincha.credito.automotriz.domain.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

@Converter(autoApply = true)
public class VehicleTypeConverter implements AttributeConverter<VehicleType, String> {
    @Override
    public String convertToDatabaseColumn(VehicleType vehicleType) {
        if (vehicleType == null) {
            return null;
        }
        return vehicleType.getType();
    }

    @Override
    public VehicleType convertToEntityAttribute(String vehicleType) {
        if (vehicleType == null) {
            return null;
        }

        return Stream.of(VehicleType.values())
                .filter(vt -> vt.getType().equals(vehicleType))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
