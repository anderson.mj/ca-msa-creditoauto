package com.bancopichincha.credito.automotriz.domain.model;

import com.bancopichincha.credito.automotriz.domain.enums.CivilStatus;
import com.bancopichincha.credito.automotriz.util.validations.EnumValidator;
import lombok.*;

import org.springframework.data.relational.core.mapping.Table;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.bancopichincha.credito.automotriz.util.RegexMessage.*;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table("client")
public class Client extends Person {

    public Client(int id) {
        super(id);
    }

    public Client(Person person, CivilStatus civilStatus, String spouseIdentification, String spouseName, boolean subjectToCredit) {
        super(person);
        this.civilStatus = civilStatus;
        this.spouseIdentification = spouseIdentification;
        this.spouseName = spouseName;
        this.subjectToCredit = subjectToCredit;

    }

    @EnumValidator(regexp = CIVIL_STATUS_REGEX, message = CIVIL_STATUS_MESSAGE)
    @Enumerated(EnumType.STRING)
    private CivilStatus civilStatus;

    private String spouseIdentification = "";

    private String spouseName = "";

    private boolean subjectToCredit = false;

    @OneToMany(mappedBy = "client", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE},
            fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Credit> credits = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return subjectToCredit == client.subjectToCredit && civilStatus == client.civilStatus && Objects.equals(spouseIdentification, client.spouseIdentification) && Objects.equals(spouseName, client.spouseName) && Objects.equals(credits, client.credits);
    }

    @Override
    public int hashCode() {
        return Objects.hash(civilStatus, spouseIdentification, spouseName, subjectToCredit, credits);
    }
}
