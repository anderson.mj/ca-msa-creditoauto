package com.bancopichincha.credito.automotriz.domain.model;

import com.bancopichincha.credito.automotriz.domain.enums.CreditState;
import com.bancopichincha.credito.automotriz.util.validations.EnumValidator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import org.springframework.data.relational.core.mapping.Table;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

import static com.bancopichincha.credito.automotriz.util.RegexMessage.*;

@Getter
@Setter
@Table("credit")
@AllArgsConstructor
@Entity
public class Credit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JsonSerialize(using = ToStringSerializer.class)
    @Column(nullable = false)
    private LocalDateTime createDate = LocalDateTime.now();

    @ManyToOne
    @NotNull(message = "Debe existir un cliente")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JoinColumn(name = "client_id", nullable = false)
    private Client client;

    @ManyToOne
    @NotNull(message = "Debe existir un patio")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JoinColumn(name = "patio_id", nullable = false)
    private Patio patio;

    @ManyToOne
    @NotNull(message = "Debe existir un vehiculo")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JoinColumn(name = "vehicle_id", nullable = false)
    private Vehicle vehicle;

    @Column(nullable = false)
    private int termMonths;

    @Column(nullable = false)
    private int quota;

    @Column(nullable = false)
    private int entrance;

    @ManyToOne
    @NotNull(message = "Debe existir un ejecutivo")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JoinColumn(name = "executive_id", nullable = false)
    private Executive executive;

    @Column(length = 1000)
    private String observations;

    @EnumValidator(regexp = CREDIT_STATE_REGEX, message = CREDIT_STATE_MESSAGE)
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private CreditState state = CreditState.REGISTERED;

}
