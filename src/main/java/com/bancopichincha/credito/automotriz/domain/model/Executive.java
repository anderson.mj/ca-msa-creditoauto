package com.bancopichincha.credito.automotriz.domain.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.data.relational.core.mapping.Table;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table("executive")
public class Executive extends Person {

    public Executive(int id) {
        super(id);
    }

    public Executive(Person person, String phoneNumber, Patio patio) {
        super(person);
        this.phoneNumber = phoneNumber;
        this.patio = patio;
    }

    @Column(length = 20)
    private String phoneNumber;

    @ManyToOne
    @JoinColumn(name = "patio_id")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Patio patio;

    @OneToMany(mappedBy = "executive", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE},
            fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Credit> credits = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Executive executive = (Executive) o;
        return Objects.equals(phoneNumber, executive.phoneNumber) && Objects.equals(patio, executive.patio) && Objects.equals(credits, executive.credits);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), phoneNumber, patio, credits);
    }
}
