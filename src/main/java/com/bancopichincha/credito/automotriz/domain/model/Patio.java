package com.bancopichincha.credito.automotriz.domain.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import org.springframework.data.relational.core.mapping.Table;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Table("patio")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Patio {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false, length = 50, unique = true)
    private String name;

    @Column(nullable = false, length = 100)
    private String address;

    @Column(nullable = false, length = 20)
    private String telephone;

    @Column(nullable = false, length = 50)
    private String pointSaleNumber;

    @OneToMany(mappedBy = "patio", fetch = FetchType.LAZY)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<Executive> executives = new ArrayList<>();

    @OneToMany(mappedBy = "patio", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE},
            fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Credit> credits = new ArrayList<>();
}
