package com.bancopichincha.credito.automotriz.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.springframework.data.relational.core.mapping.Table;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Table("patio_client")
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class PatioClient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    private Client client;

    @ManyToOne
    private Patio patio;

    @Column(nullable = false)
    private LocalDateTime assignmentDate = LocalDateTime.now();
}
