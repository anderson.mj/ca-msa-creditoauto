package com.bancopichincha.credito.automotriz.domain.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Person {

    public Person(int id) {
        this.id = id;
    }

    public Person(Person person) {
        this.id = person.getId();
        this.identification = person.getIdentification();
        this.firstNames = person.getFirstNames();
        this.lastNames = person.getLastNames();
        this.address = person.getAddress();
        this.telephone = person.getTelephone();
        this.dateBirth = person.getDateBirth();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false, unique = true, length = 20)
    private String identification;

    @Column(nullable = false, length = 50)
    private String firstNames;

    @Column(nullable = false, length = 50)
    private String lastNames;

    @Column(nullable = false, length = 100)
    private String address;

    @Column(nullable = false, length = 20)
    private String telephone;

    @JsonSerialize(using = ToStringSerializer.class)
    @Column(nullable = false)
    private LocalDateTime dateBirth;
}
