package com.bancopichincha.credito.automotriz.domain.model;

import com.bancopichincha.credito.automotriz.domain.enums.VehicleState;
import com.bancopichincha.credito.automotriz.domain.enums.VehicleType;
import com.bancopichincha.credito.automotriz.util.validations.EnumValidator;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import org.springframework.data.relational.core.mapping.Table;

import java.util.ArrayList;
import java.util.List;

import static com.bancopichincha.credito.automotriz.util.RegexMessage.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table("vehicle")
@Entity
public class Vehicle {

    public Vehicle(int id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false, length = 6, unique = true)
    private String placa;

    @Column(nullable = false, length = 50)
    private String model;

    @Column(nullable = false, length = 100)
    private String chassisNumber;

    @ManyToOne
    @NotNull(message = "Debe existir una marca")
    @JoinColumn(name = "brand_id", nullable = false)
    private Brand brand;

    @EnumValidator(regexp = VEHICLE_TYPE_REGEX, message = VEHICLE_TYPE_MESSAGE)
    @Enumerated(EnumType.STRING)
    private VehicleType type;

    @Column(nullable = false, length = 50)
    private String cylinder;

    @Column(nullable = false)
    private double valuation;

    @EnumValidator(regexp = VEHICLE_STATE_REGEX, message = VEHICLE_STATE_MESSAGE)
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private VehicleState state = VehicleState.FREE;

    @OneToMany(mappedBy = "vehicle", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE},
            fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Credit> credits = new ArrayList<>();
}
