package com.bancopichincha.credito.automotriz.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class BusinessRulesException extends RuntimeException{

    public BusinessRulesException(String message) {
        super("Business Rule break: " + message, null, false, false);
    }
}
