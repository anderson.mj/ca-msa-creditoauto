package com.bancopichincha.credito.automotriz.repository.impl;

import com.bancopichincha.credito.automotriz.domain.model.Credit;
import com.bancopichincha.credito.automotriz.service.dto.CreditDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

@Repository
public interface CreditRepository extends JpaRepository<Credit, Integer> {

    @Query("SELECT new com.bancopichincha.credito.automotriz.service.dto.CreditDto(c) FROM Credit c")
    List<CreditDto> findAllDto();

    @Query("SELECT new com.bancopichincha.credito.automotriz.service.dto.CreditDto(c) " +
            "FROM Credit c " +
            "WHERE c.id=:id")
    Optional<CreditDto> findByIdDto(@PathVariable("id")Integer id);

    @Query("SELECT count(*) " +
            "FROM Credit c " +
            "WHERE c.vehicle.id=:vehicleId " +
            "AND c.state=:state1 " +
            "OR c.state=:state2")
    int findCreditByVehicleIdReserved(@PathVariable("vehicleId") Integer vehicleId,
                                      @PathVariable("state1") String state1);

    List<Credit> findCreditByVehicleId(@PathVariable("vehicleId") Integer vehicleId);
}
