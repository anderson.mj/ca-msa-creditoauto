package com.bancopichincha.credito.automotriz.repository.impl;

import com.bancopichincha.credito.automotriz.domain.model.Executive;
import com.bancopichincha.credito.automotriz.service.dto.ExecutiveDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ExecutiveRepository extends JpaRepository<Executive, Integer> {
    Optional<Object> findByIdentification(String identification);

    @Query("SELECT new com.bancopichincha.credito.automotriz.service.dto.ExecutiveDto(e) FROM Executive e")
    public List<ExecutiveDto> findAllDto();

    @Query("SELECT new com.bancopichincha.credito.automotriz.service.dto.ExecutiveDto(e) " +
            " FROM Executive e " +
            " WHERE e.id=:id")
    public Optional<ExecutiveDto> findByIdDto(@Param("id") Integer id);
}
