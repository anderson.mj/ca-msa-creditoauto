package com.bancopichincha.credito.automotriz.repository.impl;

import com.bancopichincha.credito.automotriz.domain.model.PatioClient;
import com.bancopichincha.credito.automotriz.service.dto.PatioClientDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

@Repository
public interface PatioClientRepository extends JpaRepository<PatioClient, Integer> {

    PatioClient getByClientIdAndPatioId(int clientId, int patioId);

    @Query("SELECT new com.bancopichincha.credito.automotriz.service.dto.PatioClientDto(pc) FROM PatioClient pc")
    public List<PatioClientDto> findAllDto();

    @Query("SELECT new com.bancopichincha.credito.automotriz.service.dto.PatioClientDto(pc) " +
            "FROM PatioClient pc " +
            "WHERE pc.id=:id")
    public Optional<PatioClientDto> findByIdDto(@PathVariable("id")Integer id);

    @Query("SELECT count(*) " +
            "FROM PatioClient pc " +
            "WHERE pc.patio.id=:patioId")
    int findByPatioId(@PathVariable("patioId") Integer patioId);

    @Query("SELECT count(*) " +
            "FROM PatioClient pc " +
            "WHERE pc.client.id=:clientId")
    int findByClientId(@PathVariable("clientId") Integer clientId);
}
