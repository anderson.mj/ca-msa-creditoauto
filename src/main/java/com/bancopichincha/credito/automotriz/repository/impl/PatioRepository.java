package com.bancopichincha.credito.automotriz.repository.impl;

import com.bancopichincha.credito.automotriz.domain.model.Patio;
import com.bancopichincha.credito.automotriz.service.dto.PatioDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

@Repository
public interface PatioRepository extends JpaRepository<Patio, Integer> {
    Optional<Object> findByName(String name);

    @Query("SELECT new com.bancopichincha.credito.automotriz.service.dto.PatioDto(p) " +
            "FROM Patio p ")
    public List<PatioDto> findAllDto();

    @Query("SELECT new com.bancopichincha.credito.automotriz.service.dto.PatioDto(p) " +
            "FROM Patio p " +
            "WHERE p.id=:id")
    public Optional<PatioDto> findPatioByIdDto(@PathVariable("id") Integer id);
}
