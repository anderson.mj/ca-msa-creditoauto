package com.bancopichincha.credito.automotriz.repository.impl;

import com.bancopichincha.credito.automotriz.domain.model.Vehicle;
import com.bancopichincha.credito.automotriz.service.dto.VehicleDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, Integer> {
    Optional<Object> findByPlaca(String placa);

    @Query("SELECT new com.bancopichincha.credito.automotriz.service.dto.VehicleDto(v) FROM Vehicle v")
    List<VehicleDto> findAllDto();

    @Query("SELECT new com.bancopichincha.credito.automotriz.service.dto.VehicleDto(v) " +
            "FROM Vehicle v " +
            "WHERE v.id=:id")
    Optional<VehicleDto> findByIdDto(@PathVariable("id")Integer id);

    @Query("SELECT new com.bancopichincha.credito.automotriz.service.dto.VehicleDto(v) " +
            "FROM Vehicle v " +
            "WHERE v.brand.id=:brandId")
    List<VehicleDto> findByBrandIdDto(@PathVariable("brandId") Integer brandId);

    @Query("SELECT new com.bancopichincha.credito.automotriz.service.dto.VehicleDto(v) " +
            "FROM Vehicle v " +
            "WHERE v.model=:model")
    List<VehicleDto> findByModelDto(@PathVariable("model") String model);
}
