package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.domain.model.Brand;
import com.bancopichincha.credito.automotriz.service.dto.BrandDto;

import java.util.List;

public interface BrandService extends Crud<BrandDto> {
    void addAll(List<Brand> brands);
}
