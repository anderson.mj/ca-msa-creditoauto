package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.domain.model.Client;
import com.bancopichincha.credito.automotriz.service.dto.ClientDto;

import java.util.List;

public interface ClientService extends Crud<ClientDto> {
    void addAll(List<Client> clients);
}
