package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.service.dto.CreditDto;

public interface CreditService extends Crud<CreditDto> {

    int findCreditVehicleReserved(Integer vehicleId);
}
