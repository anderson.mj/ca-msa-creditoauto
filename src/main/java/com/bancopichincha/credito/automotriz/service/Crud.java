package com.bancopichincha.credito.automotriz.service;

import java.util.List;
import java.util.Map;

public interface Crud<T> {
    T create(T t);

    T update(T t);

    T edit(Integer id, Map<Object, Object> fields);

    void deleteById(Integer id);

    List<T> getList();

    T getById(Integer id);

    void existById(Integer id);
}
