package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.domain.model.Client;
import com.bancopichincha.credito.automotriz.domain.model.Executive;
import com.bancopichincha.credito.automotriz.service.dto.ExecutiveDto;

import java.util.List;

public interface ExecutiveService extends Crud<ExecutiveDto> {

    void addAll(List<Executive> executives);
}
