package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.domain.model.PatioClient;
import com.bancopichincha.credito.automotriz.service.dto.PatioClientDto;

public interface PatioClientService extends Crud<PatioClientDto> {
    void create(PatioClient patioClient);
}
