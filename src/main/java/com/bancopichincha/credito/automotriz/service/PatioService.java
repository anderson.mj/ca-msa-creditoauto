package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.domain.model.Client;
import com.bancopichincha.credito.automotriz.domain.model.Patio;
import com.bancopichincha.credito.automotriz.service.dto.PatioDto;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface PatioService extends Crud<PatioDto> {

    @Transactional
    PatioDto getByIdDto(Integer id);

    void addAll(List<Patio> patios);
}
