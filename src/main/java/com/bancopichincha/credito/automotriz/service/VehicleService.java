package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.service.dto.VehicleDto;

import java.util.List;

public interface VehicleService extends Crud<VehicleDto> {
    List<VehicleDto> getByBrandId(Integer brandId);

    List<VehicleDto> getByModel(String model);
}
