package com.bancopichincha.credito.automotriz.service.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BrandDto {

    private int id;

    @NotNull(message = "La marca debe tener algún nombre")
    private String name;
}
