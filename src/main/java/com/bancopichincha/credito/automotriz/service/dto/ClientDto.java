package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.domain.enums.CivilStatus;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Getter
@Setter
public class ClientDto {

    @NotNull(message = "Se debe de indicar el id del cliente")
    private int id;

    @NotNull(message = "Se debe ingresar una identificacion")
    @Size(min = 1, max = 20, message = "La longitud maxima para una identificacion es de 20")
    private String identification;

    @NotNull(message = "Se debe ingresar un nombre")
    @Size(min = 1, max = 20, message = "La longitud maxima para un nombre es de 50")
    private String firstNames;

    @NotNull(message = "Se debe ingresar un apellido")
    @Size(min = 1, max = 20, message = "La longitud maxima para un apellido es de 50")
    private String lastNames;

    @NotNull(message = "Se debe ingresar una direccion")
    @Size(min = 1, max = 100, message = "La longitud maxima para una direccion es de 100")
    private String address;

    @NotNull(message = "Se debe ingresar un telefono")
    @Size(min = 1, max = 20, message = "La longitud maxima para un telefono es de 20")
    private String telephone;

    private LocalDateTime dateBirth;

    private CivilStatus civilStatus;

    private String spouseIdentification;

    private String spouseName;

    @NotNull(message = "Se debe ingresar un estado de credito")
    private boolean subjectToCredit;
}
