package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.domain.enums.CreditState;
import com.bancopichincha.credito.automotriz.domain.model.Credit;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class CreditDto {
    @NotNull(message = "Debe existir un id del credito")
    private int id;

    @NotNull(message = "Debe existir una fecha de creacion")
    private LocalDateTime createDate;

    @NotNull(message = "Se debe especificar el cliente")
    private ClientDto client;

    @NotNull(message = "Se debe especificar el patio")
    private PatioDto patio;

    @NotNull(message = "Se debe especificar el vehiculo")
    private VehicleDto vehicle;

    @NotNull(message = "Se debe especificar termino de meses")
    private int termMonths;

    @NotNull(message = "Se debe especificar el numero de cuotas")
    private int quota;

    @NotNull(message = "Se debe especificar la entrada")
    private int entrance;

    @NotNull(message = "Se debe especificar el ejecutivo")
    private ExecutiveDto executive;

    private String observations;

    @NotNull(message = "Se debe especificar el estado del credito")
    private CreditState state;

    public CreditDto(Credit credit) {
        this.client = new ClientDto();
        this.patio = new PatioDto();
        this.vehicle = new VehicleDto();
        this.executive = new ExecutiveDto();

        this.id = credit.getId();
        this.createDate = credit.getCreateDate();
        this.client.setId(credit.getClient().getId());
        this.patio.setId(credit.getPatio().getId());
        this.vehicle.setId(credit.getVehicle().getId());
        this.termMonths = credit.getTermMonths();
        this.quota = credit.getQuota();
        this.entrance = credit.getEntrance();
        this.executive.setId(credit.getExecutive().getId());
        this.observations = credit.getObservations();
        this.state = credit.getState();
    }
}
