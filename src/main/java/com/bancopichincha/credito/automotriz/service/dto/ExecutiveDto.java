package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.domain.model.Executive;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class ExecutiveDto {
    @NotNull(message = "Se debe de indicar el id del ejecutivo")
    private int id;

    @NotNull(message = "Se debe ingresar una identificacion")
    @Size(min = 1, max = 20, message = "La longitud maxima para una identificacion es de 20")
    private String identification;

    @NotNull(message = "Se debe ingresar un nombre")
    @Size(min = 1, max = 20, message = "La longitud maxima para un nombre es de 50")
    private String firstNames;

    @NotNull(message = "Se debe ingresar un apellido")
    @Size(min = 1, max = 20, message = "La longitud maxima para un apellido es de 50")
    private String lastNames;

    @NotNull(message = "Se debe ingresar una direccion")
    @Size(min = 1, max = 100, message = "La longitud maxima para una direccion es de 100")
    private String address;

    @NotNull(message = "Se debe ingresar un telefono")
    @Size(min = 1, max = 20, message = "La longitud maxima para un telefono es de 20")
    private String telephone;

    private LocalDateTime dateBirth;

    @NotNull(message = "Se debe ingresar un telefono")
    @Size(min = 1, max = 20, message = "La longitud maxima para un numero de celular es de 20")
    private String phoneNumber;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private PatioDto patio;

    public ExecutiveDto(Executive executive) {
        this.id = executive.getId();
        this.identification = executive.getIdentification();
        this.firstNames = executive.getFirstNames();
        this.lastNames = executive.getLastNames();
        this.address = executive.getAddress();
        this.telephone = executive.getTelephone();
        this.dateBirth = executive.getDateBirth();
        this.phoneNumber = executive.getPhoneNumber();
    }
}
