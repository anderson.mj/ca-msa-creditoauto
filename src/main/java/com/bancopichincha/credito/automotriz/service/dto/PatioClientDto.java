package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.domain.model.PatioClient;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class PatioClientDto {

    private Integer id;

    @NotNull(message = "Se debe especificar el cliente")
    private ClientDto clientDto;

    @NotNull(message = "Se debe especificar el patio")
    private PatioDto patioDto;

    @NotNull(message = "Se debe especificar la fecha de asignacion")
    private LocalDateTime assignmentDate;

    public PatioClientDto(PatioClient patioClient) {
        this.clientDto = new ClientDto();
        this.patioDto = new PatioDto();
        this.id = patioClient.getId();
        this.clientDto.setId(patioClient.getClient().getId());
        this.patioDto.setId(patioClient.getPatio().getId());
        this.assignmentDate = patioClient.getAssignmentDate();
    }
}
