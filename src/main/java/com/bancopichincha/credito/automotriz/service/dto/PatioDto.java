package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.domain.model.Executive;
import com.bancopichincha.credito.automotriz.domain.model.Patio;
import com.bancopichincha.credito.automotriz.service.mapper.ExecutiveMapper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@Getter
@Setter
public class PatioDto {
    @NotNull(message = "Se debe de indicar el id del patio")
    private int id;

    @NotNull(message = "Se debe ingresar un nombre")
    @Size(min = 1, max = 20, message = "La longitud maxima para un nombre es de 50")
    private String name;

    @NotNull(message = "Se debe ingresar una direccion")
    @Size(min = 1, max = 50, message = "La longitud maxima para una direccion es de 50")
    private String address;

    @NotNull(message = "Se debe ingresar un telefono")
    @Size(min = 1, max = 20, message = "La longitud maxima para un telefono es de 20")
    private String telephone;

    @NotNull(message = "Se debe ingresar un punto de venta")
    @Size(min = 1, max = 50, message = "La longitud maxima para un punto de venta es de 50")
    private String pointSaleNumber;

//    private List<ExecutiveDto> executives = new ArrayList<>();

    public PatioDto(Patio patio) {
        this.id = patio.getId();
        this.name = patio.getName();
        this.address = patio.getAddress();
        this.telephone = patio.getTelephone();
        this.pointSaleNumber = patio.getPointSaleNumber();
//        this.executives = patio.getExecutives().stream()
//                            .map(executive -> new Executive(executive.getId()))
//                            .map(ExecutiveMapper.INSTANCE::toExecutiveDTO)
//                            .collect(Collectors.toList());
    }
}
