package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.domain.enums.VehicleState;
import com.bancopichincha.credito.automotriz.domain.enums.VehicleType;
import com.bancopichincha.credito.automotriz.domain.model.Brand;
import com.bancopichincha.credito.automotriz.domain.model.Vehicle;
import com.bancopichincha.credito.automotriz.service.mapper.BrandMapper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class VehicleDto {
    @NotNull(message = "Debe existir el id del vehiculo")
    private int id;

    @NotNull(message = "Se debe ingresar una placa")
    @Size(min = 1, max = 6, message = "La longitud maxima para una placa es de 6")
    private String placa;

    @NotNull(message = "Se debe ingresar un modelo")
    @Size(min = 1, max = 50, message = "La longitud maxima para una placa es de 50")
    private String model;

    @NotNull(message = "Se debe ingresar un numeero de chasis")
    @Size(min = 1, max = 100, message = "La longitud maxima para un chasis es de 100")
    private String chassisNumber;

    @NotNull(message = "Se debe ingresar una marca")
    private BrandDto brandDto;

    @NotNull(message = "Se debe ingresar un tipo")
    private VehicleType type;

    @NotNull(message = "Se debe ingresar un cilindraje")
    @Size(min = 1, max = 50, message = "La longitud maxima para un cilindraje es de 50")
    private String cylinder;

    @NotNull(message = "Se debe ingresar el avaluo del vehiculo")
    private double valuation;

    @NotNull(message = "Se debe ingresar el estado del vehiculo")
    private VehicleState state;

//    private List<CreditDto> credits = new ArrayList<>();

    public VehicleDto(Vehicle vehicle) {
        this.brandDto = new BrandDto();
        this.id = vehicle.getId();
        this.placa = vehicle.getPlaca();
        this.model = vehicle.getModel();
        this.chassisNumber = vehicle.getChassisNumber();
        this.brandDto.setId(vehicle.getBrand().getId());
        this.type = vehicle.getType();
        this.cylinder = vehicle.getCylinder();
        this.valuation = vehicle.getValuation();
        this.state = vehicle.getState();
    }
}
