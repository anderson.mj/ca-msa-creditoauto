package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.model.Brand;
import com.bancopichincha.credito.automotriz.exception.AlreadyExistException;
import com.bancopichincha.credito.automotriz.exception.NotFoundException;
import com.bancopichincha.credito.automotriz.repository.impl.BrandRepository;
import com.bancopichincha.credito.automotriz.service.BrandService;
import com.bancopichincha.credito.automotriz.service.dto.BrandDto;
import com.bancopichincha.credito.automotriz.service.mapper.BrandMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class BrandServiceImpl implements BrandService {

    private final BrandRepository repository;

    public BrandServiceImpl(@Autowired BrandRepository repository) {
        this.repository = repository;
    }

    @Override
    public BrandDto create(BrandDto brandDto) {
        Brand brand = BrandMapper.INSTANCE.toBrand(brandDto);
        if (existByName(brand.getName()))
            throw new AlreadyExistException("brand_name: " + brand.getName());
        repository.save(brand);
        return BrandMapper.INSTANCE.toBrandDto(brand);
    }

    @Override
    public BrandDto update(BrandDto brandDto) {
        Brand brand = BrandMapper.INSTANCE.toBrand(brandDto);
        existById(brand.getId());
        if (existByName(brand.getName()))
            throw new AlreadyExistException("brand_name: " + brand.getName());
        repository.save(brand);
        return BrandMapper.INSTANCE.toBrandDto(brand);
    }

    @Override
    public BrandDto edit(Integer id, Map<Object, Object> fields) {
        existById(id);
        Brand brand = repository.findById(id).get();

        fields.forEach((key, value) -> {
            Field field = ReflectionUtils.findField(Brand.class, (String) key);
            field.setAccessible(true);
            ReflectionUtils.setField(field, brand, value);
        });

        repository.save(brand);
        return BrandMapper.INSTANCE.toBrandDto(brand)   ;
    }


    @Override
    public void deleteById(Integer id) {
        //TODO: Check if some vehicle has this brand, if throw an exception.
        existById(id);
        repository.deleteById(id);
    }

    @Override
    public List<BrandDto> getList() {
        return repository.findAll()
                .stream()
                .map(BrandMapper.INSTANCE::toBrandDto)
                .collect(Collectors.toList());
    }

    @Override
    public BrandDto getById(Integer id) {
        Brand brand = repository.findById(id)
                .orElseThrow(() -> {log.error("Not found id: " + id); throw new NotFoundException("brand_id: " + id);});
        return BrandMapper.INSTANCE.toBrandDto(brand);
    }

    @Override
    public void existById(Integer id) {
        if(repository.findById(id).isEmpty()) {
            log.error("Not found id: " + id);
            throw new NotFoundException("brand_id: " + id);
        }
    }

    public boolean existByName(String name) {
        if(repository.findByName(name).isPresent()) {
            log.error("Already exist brand_name: " + name);
            return true;
        } else
            return false;
    }

    @Override
    public void addAll(List<Brand> brands) {

        for (Brand brand : brands) {
            if (existByName(brand.getName()) )
                continue;
            repository.save(brand);
        }
    }
}
