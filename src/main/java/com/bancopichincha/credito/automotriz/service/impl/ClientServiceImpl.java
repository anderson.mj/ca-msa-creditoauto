package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.enums.CivilStatus;
import com.bancopichincha.credito.automotriz.domain.model.Client;
import com.bancopichincha.credito.automotriz.exception.AlreadyExistException;
import com.bancopichincha.credito.automotriz.exception.BusinessRulesException;
import com.bancopichincha.credito.automotriz.exception.NotFoundException;
import com.bancopichincha.credito.automotriz.repository.impl.ClientRepository;
import com.bancopichincha.credito.automotriz.repository.impl.PatioClientRepository;
import com.bancopichincha.credito.automotriz.service.ClientService;
import com.bancopichincha.credito.automotriz.service.dto.ClientDto;
import com.bancopichincha.credito.automotriz.service.mapper.ClientMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ReflectionUtils;

import static com.bancopichincha.credito.automotriz.domain.enums.CivilStatus.*;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ClientServiceImpl implements ClientService {

    private final ClientRepository repository;
    private final PatioClientRepository patioClientRepository;

    private final String ERROR_MESSAGE_CREDIT = "The client can't be deleted, have associated credits";
    private final String ERROR_MESSAGE_PATIO = "The client can't be deleted, have associated patios";

    public ClientServiceImpl(@Autowired ClientRepository repository,
                             @Autowired PatioClientRepository patioClientRepository) {
        this.repository = repository;
        this.patioClientRepository = patioClientRepository;
    }

    @Override
    public ClientDto create(ClientDto clientDto) {
        Client client = ClientMapper.INSTANCE.toClient(clientDto);
        if (existByIdentification(client.getIdentification()))
            throw new AlreadyExistException("client_identification: " + client.getIdentification());

        repository.save(client);
        return ClientMapper.INSTANCE.toClientDto(client);
    }


    @Override
    public ClientDto update(ClientDto clientDto) {
        Client client = ClientMapper.INSTANCE.toClient(clientDto);

        existById(client.getId());
        Client clientDb = repository.findById(client.getId()).get();

        // It's necessary to check if the identification is updated, if so, check that it does not already exist
        if (!client.getIdentification().equalsIgnoreCase(clientDb.getIdentification()) &&
                existByIdentification(client.getIdentification()))
            throw new AlreadyExistException("client_identification: " + client.getIdentification());

        client.setCredits(clientDb.getCredits());

        repository.save(client);

        return ClientMapper.INSTANCE.toClientDto(client);
    }

    @Override
    public ClientDto edit(Integer id, Map<Object, Object> fields) {
        existById(id);
        Client client = repository.findById(id).get();

        fields.forEach((key, value) -> {
            // It's necessary to check if the identification is updated, if so, check that it does not already exist
            if (key.toString().equalsIgnoreCase("identification") &&
                    existByIdentification((String) value))
                throw new AlreadyExistException("client_identification: " + value);

            if(value instanceof String && isCivilStatus(value)){
                client.setCivilStatus(CivilStatus.valueOf((String) value));

            } else if(String.valueOf(key).equalsIgnoreCase("dateBirth")){
                client.setDateBirth(LocalDateTime.parse((String) value));

            } else if(String.valueOf(key).equalsIgnoreCase("subjectToCredit")){
                client.setSubjectToCredit((Boolean) value);
            } else {
                Field field = ReflectionUtils.findField(Client.class, (String)key);
                field.setAccessible(true);
                ReflectionUtils.setField(field, client, value);
            }


        });

        repository.save(client);

        return ClientMapper.INSTANCE.toClientDto(client);
    }


    @Override
    @Transactional
    public void deleteById(Integer id) {
        existById(id);
        checkBusinessRules(repository.getById(id));
        repository.deleteById(id);
    }

    @Override
    public List<ClientDto> getList() {
        return repository.findAll()
                .stream()
                .map(ClientMapper.INSTANCE::toClientDto)
                .collect(Collectors.toList());
    }

    @Override
    public ClientDto getById(Integer id) {
        Client client = repository.findById(id)
                .orElseThrow(() -> {log.error("Not found id: " + id); throw new NotFoundException("client_id: " + id);});
        return ClientMapper.INSTANCE.toClientDto(client);
    }

    @Override
    public void existById(Integer id) {
        if(repository.findById(id).isEmpty()) {
            log.error("Not found id: " + id);
            throw new NotFoundException("client_id: " + id);
        }
    }

    private boolean existByIdentification(String identification) {
        if(repository.findByIdentification(identification).isPresent()) {
            log.error("Already exist client_identification: " + identification);
            return true;
        } else
            return false;
    }

    @Override
    public void addAll(List<Client> clients) {
        for (Client client : clients) {
            if (existByIdentification(client.getIdentification()) )
                continue;
            repository.save(client);
        }
    }

    private void checkBusinessRules(Client client) {
        if (!client.getCredits().isEmpty())
            throw new BusinessRulesException(ERROR_MESSAGE_CREDIT);

        int numberPatios = patioClientRepository.findByClientId(client.getId());
        if (numberPatios > 0)
            throw new BusinessRulesException(ERROR_MESSAGE_PATIO);
    }
}
