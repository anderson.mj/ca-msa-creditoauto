package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.enums.CreditState;
import com.bancopichincha.credito.automotriz.domain.model.*;
import com.bancopichincha.credito.automotriz.exception.BusinessRulesException;
import com.bancopichincha.credito.automotriz.exception.NotFoundException;
import com.bancopichincha.credito.automotriz.repository.impl.ClientRepository;
import com.bancopichincha.credito.automotriz.repository.impl.CreditRepository;
import com.bancopichincha.credito.automotriz.service.*;
import com.bancopichincha.credito.automotriz.service.dto.CreditDto;
import com.bancopichincha.credito.automotriz.service.mapper.CreditMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class CreditServiceImpl implements CreditService {

    private final CreditRepository repository;
    private final ClientService clientService;
    private final ClientRepository clientRepository;
    private final PatioService patioService;
    private final PatioClientService patioClientService;
    private final VehicleService vehicleService;
    private final ExecutiveService executiveService;

    private final String ERROR_MESSAGE_CREATE_CREDIT_SAME_DATE = "This client can't create more credits today in this patio: ";

    public CreditServiceImpl(@Autowired CreditRepository repository,
                             @Autowired ClientService clientService,
                             @Autowired ClientRepository clientRepository,
                             @Autowired PatioService patioService,
                             @Autowired PatioClientService patioClientService,
                             @Autowired VehicleService vehicleService,
                             @Autowired ExecutiveService executiveService) {
        this.repository = repository;
        this.clientService = clientService;
        this.clientRepository = clientRepository;
        this.patioService = patioService;
        this.patioClientService = patioClientService;
        this.vehicleService = vehicleService;
        this.executiveService = executiveService;
    }

    @Override
    @Transactional
    public CreditDto create(CreditDto creditDto) {
        Credit credit = CreditMapper.INSTANCE.toCredit(creditDto);
        checkExist(credit);
        checkBusinessRules(credit);
        associateClientToPatio(credit);

        repository.save(credit);

        return CreditMapper.INSTANCE.toCreditDto(credit);
    }



    @Override
    public CreditDto update(CreditDto creditDto) {
        Credit credit = CreditMapper.INSTANCE.toCredit(creditDto);

        existById(credit.getId());
        Credit creditDb = repository.findById(credit.getId()).get();

        checkExist(credit);
        associateClientToPatio(credit);

        repository.save(credit);

        return CreditMapper.INSTANCE.toCreditDto(credit);
    }

    @Override
    public CreditDto edit(Integer id, Map<Object, Object> fields) {
        existById(id);
        Credit credit = repository.findById(id).get();

        fields.forEach((key, value) -> {
            Gson gson = new Gson();
            JsonObject jsonObject = null;
            switch (String.valueOf(key)){
                case "createDate":
                    credit.setCreateDate(LocalDateTime.parse((String) value));
                    break;
                case "client":
                    jsonObject = gson.fromJson(value.toString(), JsonObject.class);
                    Client client = new Client(jsonObject.get("id").getAsInt());
                    credit.setClient(client);
                    break;
                case "patio":
                    jsonObject = gson.fromJson(value.toString(), JsonObject.class);
                    credit.setPatio(Patio.builder().id(jsonObject.get("id").getAsInt()).build());
                    break;
                case "vehicle":
                    jsonObject = gson.fromJson(value.toString(), JsonObject.class);
                    Vehicle vehicle = new Vehicle(jsonObject.get("id").getAsInt());
                    credit.setVehicle(vehicle);
                    break;
                case "termMonths":
                    credit.setTermMonths(Integer.valueOf((String) value));
                    break;
                case "fees":
                    credit.setQuota(Integer.valueOf((String) value));
                    break;
                case "entrance":
                    credit.setEntrance(Integer.valueOf((String) value));
                    break;
                case "executive":
                    jsonObject = gson.fromJson(value.toString(), JsonObject.class);
                    Executive executive = new Executive(jsonObject.get("id").getAsInt());
                    credit.setExecutive(executive);
                    break;
                case "state":
                    credit.setState(CreditState.valueOf((String) value));
                    break;
                default:
                    Field field = ReflectionUtils.findField(Credit.class, (String) key);
                    field.setAccessible(true);
                    ReflectionUtils.setField(field, credit, value);
                    break;
            }
        });

        repository.save(credit);

        return CreditMapper.INSTANCE.toCreditDto(credit);
    }

    @Override
    public void deleteById(Integer id) {
        existById(id);
        repository.deleteById(id);
    }

    @Override
    public List<CreditDto> getList() {
        return repository.findAllDto();
    }

    @Override
    public CreditDto getById(Integer id) {
        return repository.findByIdDto(id)
                .orElseThrow(() -> {log.error("Not found id: " + id); throw new NotFoundException("credit_id: " + id);});
    }

    @Override
    public void existById(Integer id) {
        if(repository.findById(id).isEmpty()) {
            log.error("Not found id: " + id);
            throw new NotFoundException("credit_id: " + id);
        }
    }

    private void associateClientToPatio(Credit credit) {
        PatioClient patioClient = new PatioClient();
        patioClient.setClient(credit.getClient());
        patioClient.setPatio(credit.getPatio());
        patioClientService.create(patioClient);
    }

    @Override
    public int findCreditVehicleReserved(Integer vehicleId) {
        return repository.findCreditByVehicleIdReserved(vehicleId, CreditState.REGISTERED.getState());
    }

    private void checkExist(Credit credit) {
        int clientId = credit.getClient().getId();
        int patioId = credit.getPatio().getId();
        int vehicleId = credit.getVehicle().getId();
        int executiveId = credit.getExecutive().getId();

        clientService.existById(clientId);
        patioService.existById(patioId);
        vehicleService.existById(vehicleId);
        executiveService.existById(executiveId);
    }

    private void checkBusinessRules(Credit credit) {
        Client client = clientRepository.getById(credit.getClient().getId());
        boolean creditCreatedToday = client.getCredits().stream()
                .filter(c -> (c.getPatio().getId() == credit.getPatio().getId()))
                .anyMatch(this::isToday);

        if (creditCreatedToday)
            throw new BusinessRulesException(ERROR_MESSAGE_CREATE_CREDIT_SAME_DATE + credit.getPatio().getId());
    }

    private boolean isToday(Credit credit) {
        LocalDate today = LocalDateTime.now().toLocalDate();
        LocalDate creditDate = credit.getCreateDate().toLocalDate();

        return (today.isEqual(creditDate));
    }
}
