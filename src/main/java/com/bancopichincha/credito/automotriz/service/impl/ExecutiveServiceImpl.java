package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.model.Executive;
import com.bancopichincha.credito.automotriz.domain.model.Patio;
import com.bancopichincha.credito.automotriz.exception.AlreadyExistException;
import com.bancopichincha.credito.automotriz.exception.NotFoundException;
import com.bancopichincha.credito.automotriz.repository.impl.ExecutiveRepository;
import com.bancopichincha.credito.automotriz.service.ExecutiveService;
import com.bancopichincha.credito.automotriz.service.dto.ExecutiveDto;
import com.bancopichincha.credito.automotriz.service.mapper.ExecutiveMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class ExecutiveServiceImpl implements ExecutiveService {

    private final ExecutiveRepository repository;

    public ExecutiveServiceImpl(@Autowired ExecutiveRepository repository) {
        this.repository = repository;
    }

    @Override
    public ExecutiveDto create(ExecutiveDto executiveDto) {
        Executive executive = ExecutiveMapper.INSTANCE.toExecutive(executiveDto);
        if (existByIdentification(executive.getIdentification()))
            throw new AlreadyExistException("executive_identification: " + executive.getIdentification());

        repository.save(executive);
        return ExecutiveMapper.INSTANCE.toExecutiveDTO(executive);
    }

    @Override
    @Transactional
    public ExecutiveDto update(ExecutiveDto executiveDto) {
        Executive executive = ExecutiveMapper.INSTANCE.toExecutive(executiveDto);

        existById(executive.getId());
        Executive executiveDb = repository.findById(executive.getId()).get();

        // It's necessary to check if the identification is updated, if so, check that it does not already exist
        if (!executive.getIdentification().equalsIgnoreCase(executiveDb.getIdentification()) &&
                existByIdentification(executive.getIdentification()))
            throw new AlreadyExistException("executive_identification: " + executive.getIdentification());

        executive.setCredits(executiveDb.getCredits());

        repository.save(executive);

        return ExecutiveMapper.INSTANCE.toExecutiveDTO(executive);
    }

    @Override
    @Transactional
    public ExecutiveDto edit(Integer id, Map<Object, Object> fields) {
        existById(id);
        Executive executive = repository.findById(id).get();

        fields.forEach((key, value) -> {
            switch (String.valueOf(key)){
                case "dateBirth":
                    executive.setDateBirth(LocalDateTime.parse((String) value));
                break;
                case "patio":
                    Gson gson = new Gson();
                    JsonObject jsonObject = gson.fromJson(value.toString(), JsonObject.class);
                    Patio patio = Patio.builder().id(jsonObject.get("id").getAsInt()).build();
                    executive.setPatio(patio);
                    break;
                default:
                    Field field = ReflectionUtils.findField(Executive.class, (String) key);
                    field.setAccessible(true);
                    ReflectionUtils.setField(field, executive, value);
                    break;
            }
        });

        repository.save(executive);

        return ExecutiveMapper.INSTANCE.toExecutiveDTO(executive);
    }

    @Override
    public void deleteById(Integer id) {
         existById(id);
         repository.deleteById(id);
    }

    @Override
    @Transactional
    public List<ExecutiveDto> getList() {
        return repository.findAllDto();
    }

    @Override
    public ExecutiveDto getById(Integer id) {
        return repository.findByIdDto(id)
                .orElseThrow(() -> {log.error("Not found id: " + id); throw new NotFoundException("executive_id: " + id);});
    }

    @Override
    public void existById(Integer id) {
        if(repository.findById(id).isEmpty()) {
            log.error("Not found id: " + id);
            throw new NotFoundException("executive_id: " + id);
        }
    }

    private boolean existByIdentification(String identification) {
        if(repository.findByIdentification(identification).isPresent()) {
            log.error("Already exist executive_identification: " + identification);
            return true;
        } else
            return false;
    }

    @Override
    public void addAll(List<Executive> executives) {
        for (Executive executive : executives) {
            if (existByIdentification(executive.getIdentification()) )
                continue;
            repository.save(executive);
        }
    }
}
