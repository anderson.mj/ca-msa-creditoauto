package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.model.Brand;
import com.bancopichincha.credito.automotriz.domain.model.Client;
import com.bancopichincha.credito.automotriz.domain.model.Executive;
import com.bancopichincha.credito.automotriz.domain.model.Patio;
import com.bancopichincha.credito.automotriz.service.BrandService;
import com.bancopichincha.credito.automotriz.service.ClientService;
import com.bancopichincha.credito.automotriz.service.ExecutiveService;
import com.bancopichincha.credito.automotriz.service.PatioService;
import com.bancopichincha.credito.automotriz.util.MyCsvReader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class LoadDataImpl {

    private final BrandService brandService;
    private final ClientService clientService;
    private final PatioService patioService;
    private final ExecutiveService executiveService;
    private final MyCsvReader csvReader;

    public LoadDataImpl(@Autowired BrandService brandService,
                        @Autowired ClientService clientService,
                        @Autowired PatioService patioService,
                        @Autowired ExecutiveService executiveService,
                        @Autowired MyCsvReader csvReader) {
        this.brandService = brandService;
        this.clientService = clientService;
        this.patioService = patioService;
        this.executiveService = executiveService;
        this.csvReader = csvReader;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        try {
            loadBrands();
            loadClients();
            loadPatios();
            loadExecutive();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }



    private void loadBrands() throws IOException {
        ClassPathResource resource = new ClassPathResource("/data/load_brands.csv");
        File file = new File(resource.getURI());
        List<Brand> brands = new ArrayList<>();
        csvReader.readBrands(file, brands);
        brandService.addAll(brands);
    }

    private void loadClients() throws IOException {
        ClassPathResource resource = new ClassPathResource("/data/load_clients.csv");
        File file = new File(resource.getURI());
        List<Client> clients = new ArrayList<>();
        csvReader.readClients(file, clients);
        clientService.addAll(clients);

    }

    private void loadPatios() throws IOException {
        ClassPathResource resource = new ClassPathResource("/data/load_patios.csv");
        File file = new File(resource.getURI());
        List<Patio> patios = new ArrayList<>();
        csvReader.readPatios(file, patios);
        patioService.addAll(patios);
    }

    private void loadExecutive() throws IOException {
        ClassPathResource resource = new ClassPathResource("/data/load_executives.csv");
        File file = new File(resource.getURI());
        List<Executive> executives = new ArrayList<>();
        csvReader.readExecutive(file, executives);
        executiveService.addAll(executives);
    }
}
