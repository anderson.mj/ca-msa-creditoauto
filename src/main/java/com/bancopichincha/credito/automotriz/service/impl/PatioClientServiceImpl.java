package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.model.Client;
import com.bancopichincha.credito.automotriz.domain.model.Patio;
import com.bancopichincha.credito.automotriz.domain.model.PatioClient;
import com.bancopichincha.credito.automotriz.exception.AlreadyExistException;
import com.bancopichincha.credito.automotriz.exception.NotFoundException;
import com.bancopichincha.credito.automotriz.repository.impl.PatioClientRepository;
import com.bancopichincha.credito.automotriz.service.ClientService;
import com.bancopichincha.credito.automotriz.service.PatioClientService;
import com.bancopichincha.credito.automotriz.service.PatioService;
import com.bancopichincha.credito.automotriz.service.dto.PatioClientDto;
import com.bancopichincha.credito.automotriz.service.mapper.PatioClientMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
@Service
public class PatioClientServiceImpl implements PatioClientService {

    private final PatioClientRepository repository;
    private final ClientService clientService;
    private final PatioService patioService;

    public PatioClientServiceImpl(@Autowired PatioClientRepository repository,
                                  @Autowired ClientService clientService,
                                  @Autowired PatioService patioService) {
        this.repository = repository;
        this.clientService = clientService;
        this.patioService = patioService;
    }

    @Override
    public PatioClientDto create(PatioClientDto patioClientDto) {
        PatioClient patioClient = PatioClientMapper.INSTANCE.toPatioClient(patioClientDto);
        if (alreadyExistInPatio(patioClient))
            throw new AlreadyExistException("The client: " + patioClient.getClient().getId() +
                                            " Already exist in patio: " + patioClient.getPatio().getId());

        repository.save(patioClient);

        return PatioClientMapper.INSTANCE.toPatioClientDto(patioClient);
    }

    @Override
    public void create(PatioClient patioClient) {
        if (alreadyExistInPatio(patioClient))
            return;

        repository.save(patioClient);
    }


    @Override
    public PatioClientDto update(PatioClientDto patioClientDto) {
        PatioClient patioClient = PatioClientMapper.INSTANCE.toPatioClient(patioClientDto);
        if (alreadyExistInPatio(patioClient))
            throw new AlreadyExistException("The client: " + patioClient.getClient().getId() +
                    " Already exist in patio: " + patioClient.getPatio().getId());

        repository.save(patioClient);

        return PatioClientMapper.INSTANCE.toPatioClientDto(patioClient);
    }


    @Override
    @Transactional
    public PatioClientDto edit(Integer id, Map<Object, Object> fields) {
        existById(id);
        PatioClient patioClient = repository.findById(id).get();

        //This allows to check alreadyExistInPatio
        AtomicBoolean changePatio = new AtomicBoolean(false);

        fields.forEach((key, value) -> {
            Gson gson = new Gson();
            JsonObject jsonObject = null;
            switch (String.valueOf(key)) {
                case "clientDto":
                    jsonObject = gson.fromJson(value.toString(), JsonObject.class);
                    Client client = new Client(jsonObject.get("id").getAsInt());
                    patioClient.setClient(client);
                    break;
                case "patioDto":
                    jsonObject = gson.fromJson(value.toString(), JsonObject.class);
                    Patio patio = Patio.builder().id(jsonObject.get("id").getAsInt()).build();
                    patioClient.setPatio(patio);
                    changePatio.set(true);
                    break;
                case "assignmentDate":
                    patioClient.setAssignmentDate(LocalDateTime.parse((String) value));
                    break;
            }
        });

        if(changePatio.get() && alreadyExistInPatio(patioClient))
            throw new AlreadyExistException("The client: " + patioClient.getClient().getId() +
                    " Already exist in patio: " + patioClient.getPatio().getId());

        repository.save(patioClient);
        return PatioClientMapper.INSTANCE.toPatioClientDto(patioClient);
    }

    @Override
    public void deleteById(Integer id) {
        existById(id);
        repository.deleteById(id);
    }

    @Override
    public List<PatioClientDto> getList() {
        return repository.findAllDto();
    }

    @Override
    public PatioClientDto getById(Integer id) {
        return repository.findByIdDto(id)
                .orElseThrow(() -> {log.error("Not found id: " + id); throw new NotFoundException("patioCLient_id: " + id);});
    }

    @Override
    public void existById(Integer id) {
        if(repository.findById(id).isEmpty()) {
            log.error("Not found id: " + id);
            throw new NotFoundException("executive_id: " + id);
        }
    }


    private boolean alreadyExistInPatio(PatioClient patioClient) {
        int clientId = patioClient.getClient().getId();
        int patioId = patioClient.getPatio().getId();

        clientService.existById(clientId);
        patioService.existById(patioId);
        PatioClient patioClientDb = repository.getByClientIdAndPatioId(clientId, patioId);

        return (patioClientDb != null);
    }
}
