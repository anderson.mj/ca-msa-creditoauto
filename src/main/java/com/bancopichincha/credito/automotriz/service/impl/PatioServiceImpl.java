package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.model.Patio;
import com.bancopichincha.credito.automotriz.exception.AlreadyExistException;
import com.bancopichincha.credito.automotriz.exception.BusinessRulesException;
import com.bancopichincha.credito.automotriz.exception.NotFoundException;
import com.bancopichincha.credito.automotriz.repository.impl.PatioClientRepository;
import com.bancopichincha.credito.automotriz.repository.impl.PatioRepository;
import com.bancopichincha.credito.automotriz.service.PatioService;
import com.bancopichincha.credito.automotriz.service.dto.PatioDto;
import com.bancopichincha.credito.automotriz.service.mapper.PatioMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class PatioServiceImpl implements PatioService {

    private final PatioRepository repository;
    private final PatioClientRepository patioClientRepository;

    private final String ERROR_MESSAGE_CREDIT = "The patio cant be deleted, have associated credits";
    private final String ERROR_MESSAGE_EXECUTIVE = "The patio cant be deleted, have associated executives";
    private final String ERROR_MESSAGE_PATIO_CLIENT = "The patio cant be deleted, have associated clients";

    public PatioServiceImpl(@Autowired PatioRepository repository,
                            @Autowired PatioClientRepository patioClientRepository) {
        this.repository = repository;
        this.patioClientRepository = patioClientRepository;
    }

    @Override
    public PatioDto create(PatioDto patioDto) {
        Patio patio = PatioMapper.INSTANCE.toPatio(patioDto);
        if (existByName(patio.getName()))
            throw new AlreadyExistException("patio_name: " + patio.getName());

        repository.save(patio);
        return PatioMapper.INSTANCE.toPatioDto(patio);
    }

    @Override
    @Transactional
    public PatioDto update(PatioDto patioDto) {
        Patio patio = PatioMapper.INSTANCE.toPatio(patioDto);

        existById(patio.getId());
        Patio patioDb = repository.findById(patio.getId()).get();

        // It's necessary to check if the name is updated, if so, check if already exit
        if(!patio.getName().equalsIgnoreCase(patioDb.getName()) &&
            existByName(patio.getName()))
            throw new AlreadyExistException("patio_name: " + patio.getName());

        patio.setExecutives(patioDb.getExecutives());
        patio.setCredits(patioDb.getCredits());

        repository.save(patio);

        return PatioMapper.INSTANCE.toPatioDto(patio);
    }

    @Override
    @Transactional
    public PatioDto edit(Integer id, Map<Object, Object> fields) {
        existById(id);
        Patio patio = repository.findById(id).get();

        fields.forEach((key, value) -> {
            // It's necessary to check if the name is updated, if so, check if already exit
            if(key.toString().equalsIgnoreCase("name") &&
                existByName((String) value))
                throw new AlreadyExistException("patio_name: " + patio.getName());

            Field field = ReflectionUtils.findField(Patio.class, (String)key);
            field.setAccessible(true);
            ReflectionUtils.setField(field, patio, value);

        });

        repository.save(patio);

        return PatioMapper.INSTANCE.toPatioDto(patio);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        existById(id);
        checkBusinessRules(repository.getById(id));
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public List<PatioDto> getList() {
        return repository.findAllDto();
    }

    @Override
    @Transactional
    public PatioDto getById(Integer id) {
        Patio patio = repository.findById(id)
                .orElseThrow(() -> {log.error("Not found id: " + id); throw new NotFoundException("patio_id: " + id);});
        return PatioMapper.INSTANCE.toPatioDto(patio);
    }

    @Override
    @Transactional
    public PatioDto getByIdDto(Integer id) {
        return repository.findPatioByIdDto(id)
                .orElseThrow(() -> {log.error("Not found id: " + id); throw new NotFoundException("patio_id: " + id);});
    }

    @Override
    public void addAll(List<Patio> patios) {
        for (Patio patio : patios) {
            if (existByName(patio.getName()) )
                continue;
            repository.save(patio);
        }
    }

    @Override
    public void existById(Integer id) {
        if(repository.findById(id).isEmpty()) {
            log.error("Not found id: " + id);
            throw new NotFoundException("patio_id: " + id);
        }
    }

    private boolean existByName(String name) {
        if(repository.findByName(name).isPresent()) {
            log.error("Already exist patio_name: " + name);
            return true;
        } else
            return false;
    }

    private void checkBusinessRules(Patio patio) {
        if (!patio.getCredits().isEmpty())
            throw new BusinessRulesException(ERROR_MESSAGE_CREDIT);

        if (!patio.getExecutives().isEmpty())
            throw new BusinessRulesException(ERROR_MESSAGE_EXECUTIVE);

        int numberClients = patioClientRepository.findByPatioId(patio.getId());
        if (numberClients > 0)
            throw new BusinessRulesException(ERROR_MESSAGE_PATIO_CLIENT);
    }
}
