package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.enums.CreditState;
import com.bancopichincha.credito.automotriz.domain.enums.VehicleState;
import com.bancopichincha.credito.automotriz.domain.enums.VehicleType;
import com.bancopichincha.credito.automotriz.domain.model.Brand;
import com.bancopichincha.credito.automotriz.domain.model.Credit;
import com.bancopichincha.credito.automotriz.domain.model.Vehicle;
import com.bancopichincha.credito.automotriz.exception.AlreadyExistException;
import com.bancopichincha.credito.automotriz.exception.BusinessRulesException;
import com.bancopichincha.credito.automotriz.exception.NotFoundException;
import com.bancopichincha.credito.automotriz.repository.impl.CreditRepository;
import com.bancopichincha.credito.automotriz.repository.impl.VehicleRepository;
import com.bancopichincha.credito.automotriz.service.CreditService;
import com.bancopichincha.credito.automotriz.service.VehicleService;
import com.bancopichincha.credito.automotriz.service.dto.VehicleDto;
import com.bancopichincha.credito.automotriz.service.mapper.VehicleMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Service
public class VehicleServiceImpl implements VehicleService {

    private final VehicleRepository repository;
    private final CreditRepository creditRepository;

    private final String ERROR_MESSAGE_UPDATE = "The car is reserved, has credits in REGISTERED";
    private final String ERROR_MESSAGE_DELETE = "The car cannot be deleted, has credits in REGISTERED";

    public VehicleServiceImpl(@Autowired VehicleRepository repository, @Autowired CreditRepository creditRepository) {
        this.repository = repository;
        this.creditRepository = creditRepository;
    }

    @Override
    public VehicleDto create(VehicleDto vehicleDto) {
        Vehicle vehicle = VehicleMapper.INSTANCE.toVehicle(vehicleDto);
        if (existByPlaca(vehicle.getPlaca()))
            throw new AlreadyExistException("vehicle_placa: " + vehicle.getPlaca());

        repository.save(vehicle);
        return VehicleMapper.INSTANCE.toVehicleDto(vehicle);
    }


    @Override
    @Transactional
    public VehicleDto update(VehicleDto vehicleDto) {
        Vehicle vehicle = VehicleMapper.INSTANCE.toVehicle(vehicleDto);

        existById(vehicle.getId());
        Vehicle vehicleDb = repository.findById(vehicleDto.getId()).get();

        // It's necessary to check if the placa is updated, if so, check that it does not already exist
        if (!vehicle.getPlaca().equalsIgnoreCase(vehicleDb.getPlaca()) &&
                existByPlaca(vehicle.getPlaca()))
            throw new AlreadyExistException("vehicle_placa: " + vehicle.getPlaca());

        vehicle.setCredits(vehicleDb.getCredits());
        checkBusinessRules(vehicle, ERROR_MESSAGE_UPDATE);
        repository.save(vehicle);

        return VehicleMapper.INSTANCE.toVehicleDto(vehicle);
    }

    @Override
    @Transactional
    public VehicleDto edit(Integer id, Map<Object, Object> fields) {
        existById(id);
        Vehicle vehicle = repository.getById(id);

        fields.forEach((key, value) -> {
            // It's necessary to check if the placa is updated, if so, check that it does not already exist
            if (key.toString().equalsIgnoreCase("placa") &&
                    existByPlaca((String) value))
                throw new AlreadyExistException("vehicle_placa: " + value);

            Gson gson = new Gson();
            JsonObject jsonObject = null;

            switch (String.valueOf(key)) {
                case "brand":
                    jsonObject = gson.fromJson(value.toString(), JsonObject.class);
                    Brand brand = new Brand(jsonObject.get("id").getAsInt());
                    break;
                case "type":
                    vehicle.setType(VehicleType.valueOf((String) value));
                    break;
                case "valuation":
                    vehicle.setValuation(Double.valueOf((String) value));
                    break;
                case "state":
                    vehicle.setState(VehicleState.valueOf((String) value));
                    break;
                default:
                    Field field = ReflectionUtils.findField(Vehicle.class, (String) key);
                    field.setAccessible(true);
                    ReflectionUtils.setField(field, vehicle, value);
                    break;
            }
        });

        checkBusinessRules(vehicle, ERROR_MESSAGE_UPDATE);
        repository.save(vehicle);
        return VehicleMapper.INSTANCE.toVehicleDto(vehicle);
    }


    @Override
    @Transactional
    public void deleteById(Integer id) {
        existById(id);
        checkBusinessRules(repository.getById(id), ERROR_MESSAGE_DELETE);
        repository.deleteById(id);
    }

    @Override
    public List<VehicleDto> getList() {
        return repository.findAllDto();
    }

    @Override
    public VehicleDto getById(Integer id) {
        return repository.findByIdDto(id)
                .orElseThrow(() -> {log.error("Not found id: " + id); throw new NotFoundException("vehicle_id: " + id);});
    }

    @Override
    public void existById(Integer id) {
        if(repository.findById(id).isEmpty()) {
            log.error("Not found id: " + id);
            throw new NotFoundException("vehicle_id: " + id);
        }
    }

    private boolean existByPlaca(String placa) {
        if(repository.findByPlaca(placa).isPresent()) {
            log.error("Already exist vehicle_plata: " + placa);
            return true;
        } else
            return false;
    }

    @Override
    public List<VehicleDto> getByBrandId(Integer brandId) {
        return repository.findByBrandIdDto(brandId);
    }

    @Override
    public List<VehicleDto> getByModel(String model) {
        return repository.findByModelDto(model);
    }

    private void checkBusinessRules(Vehicle vehicle, String message) {
        List<Credit> credits = creditRepository.findCreditByVehicleId(vehicle.getId());

        boolean credistReserved = credits.stream().anyMatch(credit -> credit.getState().getState().equalsIgnoreCase(CreditState.REGISTERED.getState()));

        if(vehicle.getState().getStatus().equalsIgnoreCase(VehicleState.SOLD.getStatus()) && credistReserved)
            throw new BusinessRulesException(message);

    }
}
