package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.model.Brand;
import com.bancopichincha.credito.automotriz.service.dto.BrandDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface BrandMapper {

    BrandMapper INSTANCE = Mappers.getMapper(BrandMapper.class);

    BrandDto toBrandDto(Brand brand);

    @Mapping(target = "vehicles", ignore = true)
    Brand toBrand(BrandDto brandDto);
}
