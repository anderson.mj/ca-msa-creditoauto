package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.model.Credit;
import com.bancopichincha.credito.automotriz.service.dto.CreditDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CreditMapper {

    CreditMapper INSTANCE = Mappers.getMapper(CreditMapper.class);

    CreditDto toCreditDto(Credit credit);

    Credit toCredit(CreditDto creditDto);
}
