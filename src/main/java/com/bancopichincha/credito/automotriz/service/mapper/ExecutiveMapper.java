package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.model.Executive;
import com.bancopichincha.credito.automotriz.service.dto.ExecutiveDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ExecutiveMapper {

    ExecutiveMapper INSTANCE = Mappers.getMapper(ExecutiveMapper.class);

    ExecutiveDto toExecutiveDTO(Executive executive);

    @Mapping(target = "credits", ignore = true)
    Executive toExecutive(ExecutiveDto executiveDto);
}
