package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.model.PatioClient;
import com.bancopichincha.credito.automotriz.service.dto.PatioClientDto;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PatioClientMapper {

    PatioClientMapper INSTANCE = Mappers.getMapper(PatioClientMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(source = "clientDto", target = "client")
    @Mapping(source = "patioDto", target = "patio")
    PatioClient toPatioClient(PatioClientDto patioClientDto);

    @InheritInverseConfiguration
    PatioClientDto toPatioClientDto(PatioClient patioClient);

}
