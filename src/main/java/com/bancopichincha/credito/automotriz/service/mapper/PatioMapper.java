package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.model.Patio;
import com.bancopichincha.credito.automotriz.service.dto.PatioDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PatioMapper {

    PatioMapper INSTANCE = Mappers.getMapper(PatioMapper.class);

    PatioDto toPatioDto(Patio patio);

    @Mapping(target = "credits", ignore = true)
    Patio toPatio(PatioDto patioDto);
}
