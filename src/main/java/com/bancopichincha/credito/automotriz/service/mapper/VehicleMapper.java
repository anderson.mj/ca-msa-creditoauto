package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.model.Vehicle;
import com.bancopichincha.credito.automotriz.service.dto.VehicleDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface VehicleMapper {

    VehicleMapper INSTANCE = Mappers.getMapper(VehicleMapper.class);

    @Mapping(target = "brandDto", source = "brand")
    VehicleDto toVehicleDto(Vehicle vehicle);

    @Mapping(target = "brand", source = "brandDto")
    Vehicle toVehicle(VehicleDto vehicleDto);
}
