package com.bancopichincha.credito.automotriz.util;

import java.time.LocalDateTime;

public class Calculation {

    public static int getAgeByDate(LocalDateTime dateBirth) {
        return LocalDateTime.now().getYear() - dateBirth.getYear();
    }
}
