package com.bancopichincha.credito.automotriz.util;

import com.bancopichincha.credito.automotriz.domain.enums.CivilStatus;
import com.bancopichincha.credito.automotriz.domain.model.*;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvValidationException;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class MyCsvReader {

    public void readBrands(File file, List<Brand> brands) throws IOException {
        String [] row;
        CSVReader csvReader = null;

        try {
            csvReader = new CSVReaderBuilder(new FileReader(file))
                    .withCSVParser(new CSVParserBuilder()
                            .withSeparator(',')
                            .build())
                    .build();

            while ((row = csvReader.readNext()) != null) {
                String name = row[0].strip().replace("\uFEFF", "");
                brands.add(new Brand(name));
            }
        } catch (CsvValidationException | IOException e) {
            throw new RuntimeException(e);
        } finally {
            // Close the reader
            if (csvReader != null) {
                csvReader.close();
            }
        }
    }

    public void readClients(File file, List<Client> clients) throws IOException {
        String [] row;
        CSVReader csvReader = null;

        try {
            csvReader = new CSVReaderBuilder(new FileReader(file))
                    .withCSVParser(new CSVParserBuilder()
                            .withSeparator(',')
                            .build())
                    .build();

            // Skip labels
            csvReader.readNext();
            while ((row = csvReader.readNext()) != null) {

                Person person = getPerson(row);
                String civilStatusString    = row[6].strip().replace("\uFEFF", "");
                CivilStatus civilStatus = CivilStatus.valueOf(civilStatusString );
                String spouseIdentification = row[7].strip().replace("\uFEFF", "");
                String spouseName           = row[8].strip().replace("\uFEFF", "");
                String subjectToCreditString= row[9].strip().replace("\uFEFF", "");
                boolean subjectToCredit = Boolean.parseBoolean(subjectToCreditString);
                Client client = new Client(person, civilStatus, spouseIdentification, spouseName, subjectToCredit);
                clients.add(client);
            }
        } catch (CsvValidationException | IOException e) {
            throw new RuntimeException(e);
        } finally {
            // Close the reader
            if (csvReader != null) {
                csvReader.close();
            }
        }
    }

    public void readPatios(File file, List<Patio> patios) throws IOException {
        String [] row;
        CSVReader csvReader = null;

        try {
            csvReader = new CSVReaderBuilder(new FileReader(file))
                    .withCSVParser(new CSVParserBuilder()
                            .withSeparator(',')
                            .build())
                    .build();

            // Skip labels
            csvReader.readNext();
            while ((row = csvReader.readNext()) != null) {
                String name             = row[0].strip().replace("\uFEFF", "");
                String address          = row[1].strip().replace("\uFEFF", "");
                String telephone        = row[2].strip().replace("\uFEFF", "");
                String pointSaleNumber  = row[3].strip().replace("\uFEFF", "");
                patios.add(Patio.builder()  .name(name)
                                            .address(address)
                                            .telephone(telephone)
                                            .pointSaleNumber(pointSaleNumber)
                        .build());
            }
        } catch (CsvValidationException | IOException e) {
            throw new RuntimeException(e);
        } finally {
            // Close the reader
            if (csvReader != null) {
                csvReader.close();
            }
        }
    }


    public void readExecutive(File file, List<Executive> executives) throws IOException {
        String [] row;
        CSVReader csvReader = null;

        try {
            csvReader = new CSVReaderBuilder(new FileReader(file))
                    .withCSVParser(new CSVParserBuilder()
                            .withSeparator(',')
                            .build())
                    .build();

            // Skip labels
            csvReader.readNext();
            while ((row = csvReader.readNext()) != null) {
                Person person = getPerson(row);
                String phoneNumer = row[6].strip().replace("\uFEFF", "");
                String patioIdString = row[7].strip().replace("\uFEFF", "");
                int patioId = Integer.parseInt(patioIdString);
                Patio patio = Patio.builder().id(patioId).build();
                Executive executive = new Executive(person, phoneNumer, patio);
                executives.add(executive);
            }
        } catch (CsvValidationException | IOException e) {
            throw new RuntimeException(e);
        } finally {
            // Close the reader
            if (csvReader != null) {
                csvReader.close();
            }
        }
    }

    private Person getPerson(String[] row) {
        String identification       = row[0].strip().replace("\uFEFF", "");
        String firstNames           = row[1].strip().replace("\uFEFF", "");
        String lastNames            = row[2].strip().replace("\uFEFF", "");
        String address              = row[3].strip().replace("\uFEFF", "");
        String telephone            = row[4].strip().replace("\uFEFF", "");
        String dateBirthString      = row[5].strip().replace("\uFEFF", "");
        LocalDateTime dateBirt = LocalDateTime.parse(dateBirthString);

        return new Person(0, identification, firstNames, lastNames, address, telephone, dateBirt);
    }
}
