package com.bancopichincha.credito.automotriz.util;

import lombok.Getter;

@Getter
public class RegexMessage {
    public static final String CIVIL_STATUS_REGEX   = "(SINGLE|MARRIED|DIVORCED|WIDOWER)";
    public static final String CIVIL_STATUS_MESSAGE = "El estado civil debe ser alguno de los siguientes: Soltero, Casado, Divorciado, Viudo";
    public static final String SUBJECT_TO_CREDIT_REGEX = "(Sujeto de crédito|No sujeto de crédito)";
    public static final String SUBJECT_TO_CREDIT_MESSAGE = "El sujeto de credito debe definirse como \"Sujeto de crédito\" o \"No sujeto de crédito\"";
    public static final String VEHICLE_TYPE_REGEX = "SUV";
    public static final String VEHICLE_TYPE_MESSAGE = "El tipo de vehiculo debe ser SUV.";
    public static final String VEHICLE_STATE_REGEX = "(SOLD|FREE)";
    public static final String VEHICLE_STATE_MESSAGE = "El vehiculo debe estar en estado Vendido (SOLD) o Libre, sin vender (FREE)";
    public static final String CREDIT_STATE_REGEX = "(REGISTERED|SHIPPED|CANCELED)";
    public static final String CREDIT_STATE_MESSAGE = "El credito debe estar en un estado de Registrado, Despachado o Cancelado";
}
