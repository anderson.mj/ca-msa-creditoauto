package com.bancopichincha.credito.automotriz.util.validations;

import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

@Slf4j
public class EnumValidatorImpl implements ConstraintValidator<EnumValidator, Enum<?>> {
    private Pattern pattern;

    @Override
    public void initialize(EnumValidator annoation) {
        try {
            pattern = Pattern.compile(annoation.regexp());
        } catch (PatternSyntaxException e) {
            String message = "Given regex is invalid " + e.getMessage();
            log.error(message);
            throw new IllegalArgumentException(message);
        }
    }

    @Override
    public boolean isValid(Enum<?> value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }
        Matcher m = pattern.matcher(value.name());
        return m.matches();
    }
}
