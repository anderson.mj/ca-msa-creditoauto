package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.model.Client;
import com.bancopichincha.credito.automotriz.exception.AlreadyExistException;
import com.bancopichincha.credito.automotriz.exception.BusinessRulesException;
import com.bancopichincha.credito.automotriz.repository.impl.ClientRepository;
import com.bancopichincha.credito.automotriz.repository.impl.PatioClientRepository;
import com.bancopichincha.credito.automotriz.service.dto.ClientDto;
import com.bancopichincha.credito.automotriz.service.mapper.ClientMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static com.bancopichincha.credito.automotriz.service.impl.Data.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ClientServiceImplTest {

    @Mock
    ClientRepository repository;

    @Mock
    PatioClientRepository patioClientRepository;

    @InjectMocks
    ClientServiceImpl clientService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        repository = mock(ClientRepository.class);
        patioClientRepository = mock(PatioClientRepository.class);
        clientService = new ClientServiceImpl(repository, patioClientRepository);
    }

    @Test
    void create() {
        when(repository.findByIdentification(anyString())).thenReturn(Optional.empty());

        ClientDto clientDto = ClientMapper.INSTANCE.toClientDto(CLIENT);
        clientDto = clientService.create(clientDto);

        assertNotNull(clientDto);
    }

    @Test
    void createAlreadyExistIdentificationException() {
        when(repository.findByIdentification(anyString())).thenReturn(Optional.of(CLIENT));

        ClientDto clientDto = ClientMapper.INSTANCE.toClientDto(CLIENT);

        assertThrows(AlreadyExistException.class, () -> clientService.create(clientDto));
    }

    @Test
    void deleteById() {
        Client clientNoneCredits = CLIENT;
        clientNoneCredits.setCredits(new ArrayList<>());

        when(repository.findById(anyInt())).thenReturn(Optional.of(clientNoneCredits));
        when(repository.getById(anyInt())).thenReturn(clientNoneCredits);
        when(patioClientRepository.findByClientId(anyInt())).thenReturn(0);

        clientService.deleteById(1);

        verify(repository).deleteById(anyInt());
    }

    @Test
    void deleteByIdBusinessRuleExceptionExistCredits() {
        Client clientWithCredits = CLIENT;
        clientWithCredits.setCredits(List.of(CREDIT));

        when(repository.findById(anyInt())).thenReturn(Optional.of(clientWithCredits));
        when(repository.getById(anyInt())).thenReturn(clientWithCredits);

        assertThrows(BusinessRulesException.class, () -> clientService.deleteById(1));
    }
}