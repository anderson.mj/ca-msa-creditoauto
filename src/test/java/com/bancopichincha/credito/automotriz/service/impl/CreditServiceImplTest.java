package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.model.Client;
import com.bancopichincha.credito.automotriz.domain.model.Credit;
import com.bancopichincha.credito.automotriz.exception.BusinessRulesException;
import com.bancopichincha.credito.automotriz.repository.impl.ClientRepository;
import com.bancopichincha.credito.automotriz.repository.impl.CreditRepository;
import com.bancopichincha.credito.automotriz.service.*;
import com.bancopichincha.credito.automotriz.service.dto.CreditDto;
import com.bancopichincha.credito.automotriz.service.mapper.CreditMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.List;

import static com.bancopichincha.credito.automotriz.service.impl.Data.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CreditServiceImplTest {

    @Mock
    CreditRepository repository;
    @Mock
    ClientService clientService;
    @Mock
    ClientRepository clientRepository;
    @Mock
    PatioService patioService;
    @Mock
    PatioClientService patioClientService;
    @Mock
    VehicleService vehicleService;
    @Mock
    ExecutiveService executiveService;

    @InjectMocks
    CreditServiceImpl creditService;

    Client client;

    @BeforeEach
    void setUp() {
        repository = mock(CreditRepository.class);
        clientService = mock(ClientService.class);
        clientRepository = mock(ClientRepository.class);
        patioService = mock(PatioService.class);
        patioClientService = mock(PatioClientService.class);
        vehicleService = mock(VehicleService.class);
        executiveService = mock(ExecutiveService.class);
        creditService = new CreditServiceImpl(repository,
                clientService,
                clientRepository,
                patioService,
                patioClientService,
                vehicleService,
                executiveService);

        client = CLIENT;
    }

    @Test
    void create() {
        Credit credit = CREDIT;
        credit.setCreateDate(credit.getCreateDate().minusDays(1));
        client.setCredits(List.of(credit));

        Mockito.doNothing().when(clientService).existById(anyInt());
        Mockito.doNothing().when(patioService).existById(anyInt());
        Mockito.doNothing().when(vehicleService).existById(anyInt());
        Mockito.doNothing().when(executiveService).existById(anyInt());
        when(clientRepository.getById(anyInt())).thenReturn(client);

        CreditDto creditDto = CreditMapper.INSTANCE.toCreditDto(credit);
        creditDto = creditService.create(creditDto);

        assertNotNull(creditDto);
    }

    @Test
    void createBusinessRulesExceptionTryToCreateSameDay() {
        Credit credit = CREDIT;
        client.setCredits(List.of(credit));

        Mockito.doNothing().when(clientService).existById(anyInt());
        Mockito.doNothing().when(patioService).existById(anyInt());
        Mockito.doNothing().when(vehicleService).existById(anyInt());
        Mockito.doNothing().when(executiveService).existById(anyInt());
        when(clientRepository.getById(anyInt())).thenReturn(client);

        CreditDto creditDto = CreditMapper.INSTANCE.toCreditDto(credit);

        assertThrows(BusinessRulesException.class, () -> creditService.create(creditDto));
    }
}