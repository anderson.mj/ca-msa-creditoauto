package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.enums.CivilStatus;
import com.bancopichincha.credito.automotriz.domain.enums.CreditState;
import com.bancopichincha.credito.automotriz.domain.enums.VehicleState;
import com.bancopichincha.credito.automotriz.domain.enums.VehicleType;
import com.bancopichincha.credito.automotriz.domain.model.*;
import com.bancopichincha.credito.automotriz.service.mapper.PatioClientMapper;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Data {

    // Person
    private final static Person clientPerson = new Person(
            1,
            "123123123",
            "Yui",
            "Mashiro",
            "Address Client",
            "+57 2423452343",
            LocalDateTime.now()
    );


    private final static Person executivePerson = new Person(
            2,
            "321321321",
            "Lio",
            "Aori",
            "Address Executive",
            "+51 5234234234",
            LocalDateTime.now()
    );

    // Patio
    public final static Patio PATIO = Patio.builder()
            .id(1)
            .address("Patio Address")
            .name("Patio name")
            .pointSaleNumber("Point Sale")
            .telephone("+34 5123423412")
            .build();

    // Client
    public final static Client CLIENT = new Client(
            clientPerson,
            CivilStatus.SINGLE,
            "",
            "",
            true
    );

    // Executive
    public final static Executive EXECUTIVE = new Executive(
            executivePerson,
            "31754325234",
            PATIO
    );


    // Patio Client
    public final static PatioClient PATIO_CLIENT = new PatioClient(
            1,
            CLIENT,
            PATIO,
            LocalDateTime.now());

    // Brand
    public final static Brand BRAND = new Brand(1, "Skoda", new ArrayList<>());

    // Vehicle
    public final static Vehicle VEHICLE = new Vehicle(
            1,
            "CDJ423",
            "AR-5",
            "SEF4-SDE3",
            BRAND,
            VehicleType.SUV,
            "2.4",
            1423412345.123,
            VehicleState.FREE,
            new ArrayList<>()
    );

    public final static Vehicle VEHICLE_SOLD = new Vehicle(
            1,
            "CDJ423",
            "AR-5",
            "SEF4-SDE3",
            BRAND,
            VehicleType.SUV,
            "2.4",
            1423412345.123,
            VehicleState.SOLD,
            new ArrayList<>()
    );

    // Credit
    public final static Credit CREDIT = new Credit(
            1,
            LocalDateTime.now(),
            CLIENT,
            PATIO,
            VEHICLE,
            12,
            12,
            5,
            EXECUTIVE,
        "Observations text",
            CreditState.REGISTERED
    );
}
