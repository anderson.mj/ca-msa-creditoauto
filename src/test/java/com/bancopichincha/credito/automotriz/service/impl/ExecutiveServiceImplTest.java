package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.repository.impl.ExecutiveRepository;
import com.bancopichincha.credito.automotriz.service.dto.ExecutiveDto;
import com.bancopichincha.credito.automotriz.service.mapper.ExecutiveMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.bancopichincha.credito.automotriz.exception.AlreadyExistException;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static com.bancopichincha.credito.automotriz.service.impl.Data.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ExecutiveServiceImplTest {

    @Mock
    ExecutiveRepository repository;

    @InjectMocks
    ExecutiveServiceImpl executiveService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        repository = mock(ExecutiveRepository.class);
        executiveService = new ExecutiveServiceImpl(repository);
    }

    @Test
    void create() {
        when(repository.findByIdentification(anyString())).thenReturn(Optional.empty());

        ExecutiveDto executiveDto = ExecutiveMapper.INSTANCE.toExecutiveDTO(EXECUTIVE);
        executiveDto = executiveService.create(executiveDto);

        assertNotNull(executiveDto);
    }

    @Test
    void createAlreadyExistIdentificationException() {
        when(repository.findByIdentification(anyString())).thenReturn(Optional.of(EXECUTIVE));

        ExecutiveDto executiveDto = ExecutiveMapper.INSTANCE.toExecutiveDTO(EXECUTIVE);

        assertThrows(AlreadyExistException.class, () -> executiveService.create(executiveDto));
    }
}