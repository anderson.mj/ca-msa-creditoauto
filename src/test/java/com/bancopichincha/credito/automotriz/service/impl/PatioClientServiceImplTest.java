package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.repository.impl.PatioClientRepository;
import com.bancopichincha.credito.automotriz.service.ClientService;
import com.bancopichincha.credito.automotriz.service.PatioService;
import com.bancopichincha.credito.automotriz.service.dto.PatioClientDto;
import com.bancopichincha.credito.automotriz.service.mapper.PatioClientMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import com.bancopichincha.credito.automotriz.exception.AlreadyExistException;

import static org.mockito.ArgumentMatchers.anyInt;
import static com.bancopichincha.credito.automotriz.service.impl.Data.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PatioClientServiceImplTest {

    @Mock
    PatioClientRepository repository;

    @Mock
    ClientService clientService;

    @Mock
    PatioService patioService;

    @InjectMocks
    PatioClientServiceImpl patioClientService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        repository = mock(PatioClientRepository.class);
        clientService = mock(ClientService.class);
        patioService = mock(PatioService.class);
        patioClientService = new PatioClientServiceImpl(repository, clientService, patioService);
    }

    @Test
    void create() {

        when(repository.getByClientIdAndPatioId(anyInt(), anyInt())).thenReturn(null);

        PatioClientDto patioClientDto = PatioClientMapper.INSTANCE.toPatioClientDto(PATIO_CLIENT);
        patioClientDto = patioClientService.create(patioClientDto);

        assertNotNull(patioClientDto);
    }

    @Test
    void createAlreadyExistIdentificationException() {

        when(repository.getByClientIdAndPatioId(anyInt(), anyInt())).thenReturn(PATIO_CLIENT);

        PatioClientDto patioClientDto = PatioClientMapper.INSTANCE.toPatioClientDto(PATIO_CLIENT);

        assertThrows(AlreadyExistException.class, () -> patioClientService.create(patioClientDto));
    }
}