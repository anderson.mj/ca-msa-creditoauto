package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.model.Patio;
import com.bancopichincha.credito.automotriz.exception.AlreadyExistException;
import com.bancopichincha.credito.automotriz.exception.BusinessRulesException;
import com.bancopichincha.credito.automotriz.repository.impl.PatioClientRepository;
import com.bancopichincha.credito.automotriz.repository.impl.PatioRepository;
import com.bancopichincha.credito.automotriz.service.dto.PatioDto;
import com.bancopichincha.credito.automotriz.service.mapper.PatioMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.bancopichincha.credito.automotriz.service.impl.Data.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class PatioServiceImplTest {

    @Mock
    PatioRepository repository;

    @Mock
    PatioClientRepository patioClientRepository;

    @InjectMocks
    PatioServiceImpl patioService;

    Patio patio;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        repository = mock(PatioRepository.class);
        patioClientRepository = mock(PatioClientRepository.class);
        patioService = new PatioServiceImpl(repository, patioClientRepository);
        patio = PATIO;
        patio.setCredits(new ArrayList<>());
        patio.setExecutives(new ArrayList<>());
    }

    @Test
    void create() {
        when(repository.findByName(anyString())).thenReturn(Optional.empty());

        PatioDto patioDto = PatioMapper.INSTANCE.toPatioDto(PATIO);
        patioDto = patioService.create(patioDto);

        assertNotNull(patioDto);
    }

    @Test
    void createAlreadyExistNameException() {
        when(repository.findByName(anyString())).thenReturn(Optional.of(PATIO));

        PatioDto patioDto = PatioMapper.INSTANCE.toPatioDto(PATIO);

        assertThrows(AlreadyExistException.class, () -> patioService.create(patioDto));
    }

    @Test
    void deleteById() {
        when(repository.findById(anyInt())).thenReturn(Optional.of(PATIO));
        when(patioClientRepository.findByPatioId(anyInt())).thenReturn(0);
        when(repository.getById(anyInt())).thenReturn(patio);

        patioService.deleteById(1);

        verify(repository).deleteById(anyInt());
    }

    @Test
    void deleteByIdBusinessRulesExceptionHaveCredits() {
        patio.setCredits(List.of(CREDIT));
        when(repository.findById(anyInt())).thenReturn(Optional.of(PATIO));
        when(patioClientRepository.findByPatioId(anyInt())).thenReturn(0);
        when(repository.getById(anyInt())).thenReturn(patio);

        assertThrows(BusinessRulesException.class, () -> patioService.deleteById(1));
    }

    @Test
    void deleteByIdBusinessRulesExceptionHaveExecutives() {
        patio.setExecutives(List.of(EXECUTIVE));
        when(repository.findById(anyInt())).thenReturn(Optional.of(PATIO));
        when(patioClientRepository.findByPatioId(anyInt())).thenReturn(0);
        when(repository.getById(anyInt())).thenReturn(patio);

        assertThrows(BusinessRulesException.class, () -> patioService.deleteById(1));
    }

    @Test
    void deleteByIdBusinessRulesExceptionHaveClients() {
        when(repository.findById(anyInt())).thenReturn(Optional.of(PATIO));
        when(patioClientRepository.findByPatioId(anyInt())).thenReturn(1);
        when(repository.getById(anyInt())).thenReturn(patio);

        assertThrows(BusinessRulesException.class, () -> patioService.deleteById(1));
    }
}