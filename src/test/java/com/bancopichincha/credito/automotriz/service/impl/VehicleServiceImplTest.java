package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.exception.AlreadyExistException;
import com.bancopichincha.credito.automotriz.exception.BusinessRulesException;
import com.bancopichincha.credito.automotriz.repository.impl.CreditRepository;
import com.bancopichincha.credito.automotriz.repository.impl.VehicleRepository;
import com.bancopichincha.credito.automotriz.service.dto.VehicleDto;
import com.bancopichincha.credito.automotriz.service.mapper.VehicleMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.bancopichincha.credito.automotriz.service.impl.Data.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class VehicleServiceImplTest {

    @Mock
    VehicleRepository repository;

    @Mock
    CreditRepository creditRepository;

    @InjectMocks
    VehicleServiceImpl vehicleService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        repository = mock(VehicleRepository.class);
        creditRepository = mock(CreditRepository.class);
        vehicleService = new VehicleServiceImpl(repository, creditRepository);
    }

    @Test
    void create() {
        when(repository.findByPlaca(anyString())).thenReturn(Optional.empty());

        VehicleDto vehicleDto = VehicleMapper.INSTANCE.toVehicleDto(VEHICLE);
        vehicleDto = vehicleService.create(vehicleDto);

        assertNotNull(vehicleDto);
    }

    @Test
    void createAlreadyExistPlacaException() {
        when(repository.findByPlaca(anyString())).thenReturn(Optional.of(VEHICLE));

        VehicleDto vehicleDto = VehicleMapper.INSTANCE.toVehicleDto(VEHICLE);

        assertThrows(AlreadyExistException.class, () -> vehicleService.create(vehicleDto));
    }

    @Test
    void edit() {
        when(repository.findById(anyInt())).thenReturn(Optional.of(VEHICLE));
        when(repository.getById(anyInt())).thenReturn(VEHICLE);
        when(repository.findByPlaca(anyString())).thenReturn(Optional.empty());
        when(creditRepository.findCreditByVehicleId(anyInt())).thenReturn(new ArrayList<>());

        String newPlaca = "ABC123";
        Map<Object, Object> fields = Map.of("placa", newPlaca);

        VehicleDto vehicleDto = vehicleService.edit(1, fields);

        assertEquals(newPlaca, vehicleDto.getPlaca());
    }

    @Test
    void editBusinessRulesExceptionCantBeSold() {
        when(repository.findById(anyInt())).thenReturn(Optional.of(VEHICLE));
        when(repository.getById(anyInt())).thenReturn(VEHICLE);
        when(repository.findByPlaca(anyString())).thenReturn(Optional.empty());
        when(creditRepository.findCreditByVehicleId(anyInt())).thenReturn(List.of(CREDIT));

        Map<Object, Object> fields = Map.of("state", "SOLD");

        assertThrows(BusinessRulesException.class, () -> vehicleService.edit(1, fields));
    }

    @Test
    void deleteById() {
        when(repository.findById(anyInt())).thenReturn(Optional.of(VEHICLE));
        when(repository.getById(anyInt())).thenReturn(VEHICLE);
        when(repository.findByPlaca(anyString())).thenReturn(Optional.empty());
        when(creditRepository.findCreditByVehicleId(anyInt())).thenReturn(new ArrayList<>());

        vehicleService.deleteById(1);

        verify(repository).deleteById(anyInt());
    }

    @Test
    void deleteByIdBusinessRulesExceptionCantBeDeleted() {
        when(repository.findById(anyInt())).thenReturn(Optional.of(VEHICLE_SOLD));
        when(repository.getById(anyInt())).thenReturn(VEHICLE_SOLD);
        when(repository.findByPlaca(anyString())).thenReturn(Optional.empty());
        when(creditRepository.findCreditByVehicleId(anyInt())).thenReturn(List.of(CREDIT));

        assertThrows(BusinessRulesException.class, () -> vehicleService.deleteById(1));
    }
}